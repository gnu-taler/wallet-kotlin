/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.api

import kotlinx.coroutines.runBlocking
import net.taler.lib.common.Amount

public actual class WalletFactory {
    public actual fun createWalletApi(): WalletApi {
        return WalletApiBlocking()
    }
}

internal class WalletApiBlocking : WalletApi {

    private val api = WalletApiImpl()

    override fun getVersions(): SupportedVersions {
        return api.getVersions()
    }

    override fun getWithdrawalDetailsForUri(talerWithdrawUri: String): WithdrawalDetailsForUri = runBlocking {
        api.getWithdrawalDetailsForUri(talerWithdrawUri)
    }

    override fun getWithdrawalDetailsForAmount(exchangeBaseUrl: String, amount: Amount): WithdrawalDetails =
        runBlocking {
            api.getWithdrawalDetailsForAmount(exchangeBaseUrl, amount)
        }

    override fun listExchanges(): List<ExchangeListItem> = runBlocking {
        api.listExchanges()
    }

    override fun addExchange(exchangeBaseUrl: String): ExchangeListItem = runBlocking {
        api.addExchange(exchangeBaseUrl)
    }

    override fun getExchangeTos(exchangeBaseUrl: String): GetExchangeTosResult = runBlocking {
        api.getExchangeTos(exchangeBaseUrl)
    }

    override fun setExchangeTosAccepted(exchangeBaseUrl: String, acceptedEtag: String) = runBlocking {
        api.setExchangeTosAccepted(exchangeBaseUrl, acceptedEtag)
    }

}
