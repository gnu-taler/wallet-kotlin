/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import net.taler.lib.common.Amount

internal class CoinRecord(
    /**
     * Where did the coin come from?  Used for recouping coins.
     */
    val coinSource: CoinSourceType,

    /**
     * Public key of the coin.
     */
    val coinPub: String,

    /**
     * Private key to authorize operations on the coin.
     */
    val coinPriv: String,

    /**
     * Key used by the exchange used to sign the coin.
     */
    val denomPub: String,

    /**
     * Hash of the public key that signs the coin.
     */
    val denomPubHash: String,

    /**
     * Unblinded signature by the exchange.
     */
    val denomSig: String,

    /**
     * Amount that's left on the coin.
     */
    val currentAmount: Amount,

    /**
     * Base URL that identifies the exchange from which we got the coin.
     */
    val exchangeBaseUrl: String,

    /**
     * The coin is currently suspended, and will not be used for payments.
     */
    val suspended: Boolean,

    /**
     * Blinding key used when withdrawing the coin.
     * Potentially send again during payback.
     */
    val blindingKey: String,

    /**
     * Status of the coin.
     */
    val status: CoinStatus,
)

internal enum class CoinSourceType(val value: String) {
    WITHDRAW("withdraw"),
    REFRESH("refresh"),
    TIP("tip"),
}

internal enum class CoinStatus(val value: String) {

    /**
     * Withdrawn and never shown to anybody.
     */
    FRESH("fresh"),

    /**
     * A coin that has been spent and refreshed.
     */
    DORMANT("dormant"),

}
