/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.api

import net.taler.lib.common.Amount

public data class WithdrawalDetailsForUri(
    /**
     * The amount that the user wants to withdraw
     */
    val amount: Amount,

    /**
     * Exchange suggested by the wallet
     */
    val defaultExchangeBaseUrl: String?,

    /**
     * A list of exchanges that can be used for this withdrawal
     */
    val possibleExchanges: List<ExchangeListItem>,
)

public data class WithdrawalDetails(
    /**
     * Did the user accept the current version of the exchange's terms of service?
     */
    val tosAccepted: Boolean,

    /**
     * Amount that will be transferred to the exchange.
     */
    val amountRaw: Amount,

    /**
     * Amount that will be added to the user's wallet balance.
     */
    val amountEffective: Amount,
)
