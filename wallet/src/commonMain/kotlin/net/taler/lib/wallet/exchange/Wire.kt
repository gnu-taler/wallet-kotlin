/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp

@Serializable
internal data class Wire(
    val accounts: List<AccountInfo>,
    val fees: Map<String, List<WireFee>>
) {
    companion object {
        /**
         * Fetch an exchange's /wire with the given normalized base URL.
         */
        suspend fun fetch(httpClient: HttpClient, exchangeBaseUrl: String): Wire {
            return httpClient.get("${exchangeBaseUrl}wire")
        }
    }
}

@Serializable
internal data class AccountInfo(
    @SerialName("payto_uri")
    val paytoUri: String,
    @SerialName("master_sig")
    val masterSig: String,
)

/**
 * Wire fees as announced by the exchange.
 */
@Serializable
internal data class WireFee(
    /**
     * Fee for wire transfers.
     */
    @SerialName("wire_fee")
    val wireFee: Amount,
    /**
     * Fees to close and refund a reserve.
     */
    @SerialName("closing_fee")
    val closingFee: Amount,
    /**
     * Start date of the fee.
     */
    @SerialName("start_date")
    val startStamp: Timestamp,
    /**
     * End date of the fee.
     */
    @SerialName("end_date")
    val endStamp: Timestamp,
    /**
     * Signature made by the exchange master key.
     */
    @SerialName("sig")
    val signature: String,
)
