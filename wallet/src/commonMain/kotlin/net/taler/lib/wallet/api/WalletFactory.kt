/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.api

import io.ktor.client.HttpClient
import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp
import net.taler.lib.common.Version
import net.taler.lib.crypto.Crypto
import net.taler.lib.crypto.CryptoFactory
import net.taler.lib.wallet.Db
import net.taler.lib.wallet.DbFactory
import net.taler.lib.wallet.crypto.Signature
import net.taler.lib.wallet.exchange.Exchange
import net.taler.lib.wallet.getDefaultHttpClient
import net.taler.lib.wallet.operations.Withdraw


public expect class WalletFactory {
    public fun createWalletApi(): WalletApi
}

public class WalletApiImpl {

    private val httpClient: HttpClient = getDefaultHttpClient()
    private val db: Db = DbFactory().openDb()
    private val crypto: Crypto = CryptoFactory.getCrypto()
    private val signature: Signature = Signature(crypto)
    private val exchangeManager: Exchange = Exchange(crypto, signature, httpClient, db = db)
    private val withdrawManager = Withdraw(httpClient, db, crypto, signature, exchangeManager)

    public fun getVersions(): SupportedVersions {
        return SupportedVersions(
            walletVersion = Version(8, 0, 0),
            exchangeVersion = Version(8, 0, 0),
            bankVersion = Version(0, 0, 0),
            merchantVersion = Version(1, 0, 0),
        )
    }

    public suspend fun getWithdrawalDetailsForUri(talerWithdrawUri: String): WithdrawalDetailsForUri {
        val bankInfo = withdrawManager.getBankInfo(talerWithdrawUri)
        return WithdrawalDetailsForUri(
            amount = bankInfo.amount,
            defaultExchangeBaseUrl = bankInfo.suggestedExchange,
            possibleExchanges = emptyList(),
        )
    }

    public suspend fun getWithdrawalDetailsForAmount(
        exchangeBaseUrl: String,
        amount: Amount
    ): WithdrawalDetails {
        val details = withdrawManager.getWithdrawalDetails(exchangeBaseUrl, amount)
        return WithdrawalDetails(
            tosAccepted = details.exchange.termsOfServiceAccepted,
            amountRaw = amount,
            amountEffective = amount - details.overhead - details.withdrawFee,
        )
    }

    public suspend fun listExchanges(): List<ExchangeListItem> {
        return db.listExchanges().mapNotNull { exchange ->
            ExchangeListItem.fromExchangeRecord(exchange)
        }
    }

    public suspend fun addExchange(exchangeBaseUrl: String): ExchangeListItem {
        val exchange = exchangeManager.updateFromUrl(exchangeBaseUrl)
        db.put(exchange)
        return ExchangeListItem.fromExchangeRecord(exchange) ?: TODO("error handling")
    }

    public suspend fun getExchangeTos(exchangeBaseUrl: String): GetExchangeTosResult {
        val record = db.getExchangeByBaseUrl(exchangeBaseUrl) ?: TODO("error handling")
        return GetExchangeTosResult(
            tos = record.termsOfServiceText ?: TODO("error handling"),
            currentEtag = record.termsOfServiceLastEtag ?: TODO("error handling"),
            acceptedEtag = record.termsOfServiceAcceptedEtag,
        )
    }

    public suspend fun setExchangeTosAccepted(exchangeBaseUrl: String, acceptedEtag: String) {
        db.transaction {
            val record = getExchangeByBaseUrl(exchangeBaseUrl) ?: TODO("error handling")
            val updatedRecord = record.copy(
                termsOfServiceAcceptedEtag = acceptedEtag,
                termsOfServiceAcceptedTimestamp = Timestamp.now(),
            )
            put(updatedRecord)
        }
    }

}
