/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import net.taler.lib.common.Timestamp

/**
 * Exchange record as stored in the wallet's database.
 */
internal data class ExchangeRecord(
    /**
     * Base url of the exchange.
     */
    val baseUrl: String,

    /**
     * Did we finish adding the exchange?
     */
    val addComplete: Boolean = false,

    /**
     * Was the exchange added as a built-in exchange?
     */
    val builtIn: Boolean = false,

    /**
     * Details, once known.
     */
    val details: ExchangeDetails? = null,

    /**
     * Mapping from wire method type to the wire fee.
     */
    val wireInfo: ExchangeWireInfo? = null,

    /**
     * When was the exchange added to the wallet?
     */
    val timestampAdded: Timestamp,

    /**
     * Terms of service text or undefined if not downloaded yet.
     */
    val termsOfServiceText: String? = null,

    /**
     * ETag for last terms of service download.
     */
    val termsOfServiceLastEtag: String? = null,

    /**
     * ETag for last terms of service download.
     */
    val termsOfServiceAcceptedEtag: String? = null,

    /**
     * ETag for last terms of service download.
     */
    val termsOfServiceAcceptedTimestamp: Timestamp? = null,

    /**
     * Time when the update to the exchange has been started or
     * undefined if no update is in progress.
     */
    val updateStarted: Timestamp? = null,

    val updateStatus: ExchangeUpdateStatus,

    val updateReason: ExchangeUpdateReason? = null,
) {
    init {
        check(baseUrl == Exchange.normalizeUrl(baseUrl)) { "Base URL was not normalized" }
    }

    val termsOfServiceAccepted: Boolean
        get() = termsOfServiceAcceptedTimestamp != null && termsOfServiceAcceptedEtag == termsOfServiceLastEtag
}

/**
 * Details about the exchange that we only know after querying /keys and /wire.
 */
internal data class ExchangeDetails(
    /**
     * Master public key of the exchange.
     */
    val masterPublicKey: String,

    /**
     * Auditors (partially) auditing the exchange.
     */
    val auditors: List<Auditor>,

    /**
     * Currency that the exchange offers.
     */
    val currency: String,

    /**
     * Last observed protocol version.
     */
    val protocolVersion: String,

    /**
     * Signing keys we got from the exchange, can also contain
     * older signing keys that are not returned by /keys anymore.
     */
    val signingKeys: List<SigningKey>,

    /**
     * Timestamp for last update.
     */
    val lastUpdateTime: Timestamp,
)

internal data class ExchangeWireInfo(
    val feesForType: Map<String, List<WireFee>>,
    val accounts: List<ExchangeBankAccount>,
)

// TODO is this class needed?
internal data class ExchangeBankAccount(
    val paytoUri: String,
)

internal sealed class ExchangeUpdateStatus(val value: String) {
    object FetchKeys : ExchangeUpdateStatus("fetch-keys")
    object FetchWire : ExchangeUpdateStatus("fetch-wire")
    object FetchTerms : ExchangeUpdateStatus("fetch-terms")
    object FinalizeUpdate : ExchangeUpdateStatus("finalize-update")
    object Finished : ExchangeUpdateStatus("finished")
}

internal sealed class ExchangeUpdateReason(val value: String) {
    object Initial : ExchangeUpdateReason("initial")
    object Forced : ExchangeUpdateReason("forced")
    object Scheduled : ExchangeUpdateReason("scheduled")
}
