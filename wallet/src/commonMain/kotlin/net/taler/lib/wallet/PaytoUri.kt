/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

internal data class PaytoUri(
    val targetType: String,
    val targetPath: String,
    val params: Map<String, String>,
) {
    companion object {
        private const val SCHEMA = "payto://"
        fun fromString(s: String): PaytoUri? {
            if (!s.startsWith(SCHEMA)) return null
            val rest = s.slice(SCHEMA.length until s.length).split('?')
            val account = rest[0]
            val query = if (rest.size > 1) rest[1] else null
            val firstSlashPos = account.indexOf('/')
            if (firstSlashPos == -1) return null
            return PaytoUri(
                targetType = account.slice(0 until firstSlashPos),
                targetPath = account.slice((firstSlashPos + 1) until account.length),
                params = HashMap<String, String>().apply {
                    query?.split('&')?.forEach {
                        val field = it.split('=')
                        if (field.size > 1) put(field[0], field[1])
                    }
                },
            )
        } // end fromString()
    }
}
