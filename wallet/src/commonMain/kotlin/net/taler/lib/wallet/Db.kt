/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import net.taler.lib.wallet.exchange.DenominationRecord
import net.taler.lib.wallet.exchange.ExchangeRecord

internal interface Db {
    suspend fun put(exchange: ExchangeRecord)
    suspend fun listExchanges(): List<ExchangeRecord>
    suspend fun getExchangeByBaseUrl(baseUrl: String): ExchangeRecord?
    suspend fun deleteExchangeByBaseUrl(baseUrl: String)
    suspend fun put(denomination: DenominationRecord)
    suspend fun getDenominationsByBaseUrl(baseUrl: String): List<DenominationRecord>
    suspend fun <T> transaction(function: suspend Db.() -> T): T
}

internal expect class DbFactory() {
    fun openDb(): Db
}

internal class FakeDb : Db {

    private data class Data(
        val exchanges: HashMap<String, ExchangeRecord> = HashMap(),
        val denominations: HashMap<String, ArrayList<DenominationRecord>> = HashMap(),
    )

    private var data = Data()
    private val mutex = Mutex(false)

    override suspend fun put(exchange: ExchangeRecord) {
        data.exchanges[exchange.baseUrl] = exchange
    }

    override suspend fun listExchanges(): List<ExchangeRecord> {
        return data.exchanges.values.toList()
    }

    override suspend fun getExchangeByBaseUrl(baseUrl: String): ExchangeRecord? {
        return data.exchanges[baseUrl]
    }

    override suspend fun deleteExchangeByBaseUrl(baseUrl: String) {
        data.exchanges.remove(baseUrl)
    }

    override suspend fun put(denomination: DenominationRecord): Unit = mutex.withLock("transaction") {
        val list = data.denominations[denomination.exchangeBaseUrl] ?: {
            val newList = ArrayList<DenominationRecord>()
            data.denominations[denomination.exchangeBaseUrl] = newList
            newList
        }()
        val index = list.indexOfFirst { it.denomPub == denomination.denomPub }
        if (index == -1) list.add(denomination)
        else list[index] = denomination
    }

    override suspend fun getDenominationsByBaseUrl(baseUrl: String): List<DenominationRecord> {
        return data.denominations[baseUrl] ?: emptyList()
    }

    override suspend fun <T> transaction(function: suspend Db.() -> T): T = mutex.withLock("transaction") {
        val dataCopy = data.copy()
        return@withLock try {
            function()
        } catch (e: Throwable) {
            data = dataCopy
            throw e
        }
    }

}
