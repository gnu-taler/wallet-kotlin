/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import kotlinx.serialization.Serializable

/**
 * Auditor information as given by the exchange in /keys.
 */
@Serializable
internal data class Auditor(
    /**
     * Auditor's public key.
     */
    val auditor_pub: String,

    /**
     * Base URL of the auditor.
     */
    val auditor_url: String,

    /**
     * List of signatures for denominations by the auditor.
     */
    val denomination_keys: List<AuditorDenomSig>,
)

/**
 * Signature by the auditor that a particular denomination key is audited.
 */
@Serializable
internal data class AuditorDenomSig(
    /**
     * Denomination public key's hash.
     */
    val denom_pub_h: String,

    /**
     * The signature.
     */
    val auditor_sig: String,
)
