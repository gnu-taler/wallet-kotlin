/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.operations

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import net.taler.lib.common.Amount
import net.taler.lib.common.TalerUri.parseWithdrawUri
import net.taler.lib.common.Timestamp
import net.taler.lib.common.Version.VersionMatchResult
import net.taler.lib.crypto.Crypto
import net.taler.lib.crypto.CryptoFactory
import net.taler.lib.wallet.Db
import net.taler.lib.wallet.DbFactory
import net.taler.lib.wallet.crypto.Signature
import net.taler.lib.wallet.exchange.DenominationRecord
import net.taler.lib.wallet.exchange.DenominationSelectionInfo
import net.taler.lib.wallet.exchange.DenominationStatus.Unverified
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedBad
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedGood
import net.taler.lib.wallet.exchange.Exchange
import net.taler.lib.wallet.exchange.ExchangeRecord
import net.taler.lib.wallet.exchange.ExchangeWireInfo
import net.taler.lib.wallet.exchange.SelectedDenomination
import net.taler.lib.wallet.getDefaultHttpClient

internal class Withdraw(
    private val httpClient: HttpClient = getDefaultHttpClient(),
    private val db: Db = DbFactory().openDb(),
    private val crypto: Crypto = CryptoFactory.getCrypto(),
    private val signature: Signature = Signature(
        crypto
    ),
    private val exchange: Exchange = Exchange(crypto, signature, httpClient, db = db),
) {

    data class BankDetails(
        val amount: Amount,
        val selectionDone: Boolean,
        val transferDone: Boolean,
        val senderPaytoUri: String?,
        val suggestedExchange: String?,
        val confirmTransferUrl: String?,
        val wireTypes: List<String>,
        val extractedStatusUrl: String,
    )

    @Serializable
    data class Response(
        @SerialName("selection_done")
        val selectionDone: Boolean,
        @SerialName("transfer_done")
        val transferDone: Boolean,
        val amount: Amount,
        @SerialName("wire_types")
        val wireTypes: List<String>,
        @SerialName("sender_wire")
        val senderPaytoUri: String?,
        @SerialName("suggested_exchange")
        val suggestedExchange: String?,
        @SerialName("confirm_transfer_url")
        val confirmTransferUrl: String?,
    ) {
        fun toBankDetails(extractedStatusUrl: String) = BankDetails(
            amount = amount,
            confirmTransferUrl = confirmTransferUrl,
            extractedStatusUrl = extractedStatusUrl,
            selectionDone = selectionDone,
            senderPaytoUri = senderPaytoUri,
            suggestedExchange = suggestedExchange,
            transferDone = transferDone,
            wireTypes = wireTypes,
        )
    }

    suspend fun getBankInfo(talerWithdrawUri: String): BankDetails {
        val uriResult =
            parseWithdrawUri(talerWithdrawUri) ?: throw Error("Can't parse URI $talerWithdrawUri")
        val url =
            "${uriResult.bankIntegrationApiBaseUrl}api/withdraw-operation/${uriResult.withdrawalOperationId}"
        val response: Response = httpClient.get(url)
        return response.toBankDetails(url)
    }

    /**
     * Information about what will happen when creating a reserve.
     *
     * Sent to the wallet frontend to be rendered and shown to the user.
     */
    data class WithdrawalDetails(
        /**
         * Exchange that the reserve will be created at.
         */
        // TODO we probably don't need to include our internal exchange record in here
        val exchange: ExchangeRecord,

        /**
         * Selected denominations for withdraw.
         */
        val selectedDenominations: DenominationSelectionInfo,

        /**
         * Fees for withdraw.
         */
        val withdrawFee: Amount,

        /**
         * Remaining balance that is too small to be withdrawn.
         */
        val overhead: Amount,

        /**
         * The earliest deposit expiration of the selected coins.
         */
        // TODO what is this needed for?
        val earliestDepositExpiration: Timestamp,

        /**
         * Number of currently offered denominations.
         */
        // TODO what is this needed for?
        val numOfferedDenoms: Int,
    ) {
        init {
            check(exchange.details != null)
            check(exchange.wireInfo != null)
        }

        /**
         * Filtered wire info to send to the bank.
         */
        val exchangeWireAccounts: List<String> get() = exchange.wireInfo!!.accounts.map { it.paytoUri }

        /**
         * Wire fees from the exchange.
         */
        val wireFees: ExchangeWireInfo get() = exchange.wireInfo!!

        /**
         * Did the user already accept the current terms of service for the exchange?
         */
        val termsOfServiceAccepted: Boolean get() = exchange.termsOfServiceAccepted

        /**
         * Result of checking the wallet's version against the exchange's version.
         */
        val versionMatch: VersionMatchResult?
            get() = Exchange.getVersionMatch(exchange.details!!.protocolVersion)

    }

    internal suspend fun getWithdrawalDetails(
        exchangeBaseUrl: String,
        amount: Amount,
    ): WithdrawalDetails {
        val exchange = exchange.updateFromUrl(exchangeBaseUrl)
        check(exchange.details != null)
        check(exchange.wireInfo != null)
        val selectedDenominations = selectDenominations(exchange, amount)
        val possibleDenominations =
            db.getDenominationsByBaseUrl(exchangeBaseUrl).filter { it.isOffered }
        // TODO determine trust and audit status
        return WithdrawalDetails(
            exchange = exchange,
            selectedDenominations = selectedDenominations,
            withdrawFee = selectedDenominations.totalWithdrawCost - selectedDenominations.totalCoinValue,
            overhead = amount - selectedDenominations.totalWithdrawCost,
            earliestDepositExpiration = selectedDenominations.getEarliestDepositExpiry(),
            numOfferedDenoms = possibleDenominations.size,
        )
    }

    /**
     * Get a list of denominations to withdraw from the given exchange for the given amount,
     * making sure that all denominations' signatures are verified.
     */
    internal suspend fun selectDenominations(
        exchange: ExchangeRecord,
        amount: Amount,
    ): DenominationSelectionInfo {
        val exchangeDetails =
            exchange.details ?: throw Error("Exchange $exchange details not available.")

        val possibleDenominations = getPossibleDenominations(exchange.baseUrl)
        val selectedDenominations = getDenominationSelection(amount, possibleDenominations)
        // TODO consider validating denominations before writing them into the DB
        for (selectedDenomination in selectedDenominations.selectedDenominations) {
            var denomination = selectedDenomination.denominationRecord
            if (denomination.status == Unverified) {
                val valid = signature.verifyDenominationRecord(
                    denomination,
                    exchangeDetails.masterPublicKey
                )
                denomination = if (!valid) {
                    denomination.copy(status = VerifiedBad)
                } else {
                    denomination.copy(status = VerifiedGood)
                }
                db.put(denomination)
            }
            if (denomination.status == VerifiedBad) throw Error("Exchange $exchange has bad denomination.")
        }
        return selectedDenominations
    }

    suspend fun getPossibleDenominations(exchangeBaseUrl: String): List<DenominationRecord> {
        return db.getDenominationsByBaseUrl(exchangeBaseUrl).filter { denomination ->
            (denomination.status == Unverified || denomination.status == VerifiedGood) &&
                    !denomination.isRevoked
        }
    }

    /**
     * Get a list of denominations (with repetitions possible)
     * whose total value is as close as possible to the available amount, but never larger.
     *
     * Note that this algorithm does not try to optimize withdrawal fees.
     */
    fun getDenominationSelection(
        amount: Amount,
        denoms: List<DenominationRecord>,
    ): DenominationSelectionInfo {
        val selectedDenominations = ArrayList<SelectedDenomination>()
        var totalCoinValue = Amount.zero(amount.currency)
        var totalWithdrawCost = Amount.zero(amount.currency)

        // denominations need to be sorted, so we try the highest ones first
        val now = Timestamp.now()
        val denominations = denoms.filter { it.isWithdrawable(now) }.sortedByDescending { it.value }
        var remainingAmount = amount.copy()
        val zero = Amount.zero(amount.currency)
        for (d in denominations) {
            var count = 0
            val totalCost = d.value + d.feeWithdraw
            // keep adding this denomination as long as its total cost fits into remaining amount
            while (remainingAmount >= totalCost) {
                remainingAmount -= totalCost
                count++
            }
            // calculate new totals based on count-many added denominations
            if (count > 0) {
                totalCoinValue += d.value * count
                totalWithdrawCost += totalCost * count
                selectedDenominations.add(SelectedDenomination(count, d))
            }
            // stop early if nothing is remaining
            if (remainingAmount == zero) break
        }
        return DenominationSelectionInfo(
            selectedDenominations = selectedDenominations,
            totalCoinValue = totalCoinValue,
            totalWithdrawCost = totalWithdrawCost,
        )
    }

}
