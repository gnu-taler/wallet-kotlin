/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.crypto

import net.taler.lib.crypto.Crypto
import net.taler.lib.crypto.Base32Crockford
import net.taler.lib.wallet.CoinRecord
import net.taler.lib.wallet.CoinSourceType.REFRESH
import net.taler.lib.wallet.crypto.Signature.Companion.WALLET_COIN_RECOUP
import net.taler.lib.wallet.crypto.Signature.PurposeBuilder

internal class Recoup(private val crypto: Crypto) {

    /**
     * Request that we send to the exchange to get a payback.
     */
    data class Request(
        /**
         * Hashed denomination public key of the coin we want to get
         * paid back.
         */
        val denomPubHash: String,

        /**
         * Signature over the coin public key by the denomination.
         */
        val denomSig: String,

        /**
         * Coin public key of the coin we want to refund.
         */
        val coinPub: String,

        /**
         * Blinding key that was used during withdraw,
         * used to prove that we were actually withdrawing the coin.
         */
        val coinBlindKeySecret: String,

        /**
         * Signature made by the coin, authorizing the payback.
         */
        val coinSig: String,

        /**
         * Was the coin refreshed (and thus the recoup should go to the old coin)?
         */
        val refreshed: Boolean,
    )

    /**
     * Create and sign a message to recoup a coin.
     */
    fun createRequest(coin: CoinRecord): Request {
        val p = PurposeBuilder(WALLET_COIN_RECOUP)
            .put(Base32Crockford.decode(coin.coinPub))
            .put(Base32Crockford.decode(coin.denomPubHash))
            .put(Base32Crockford.decode(coin.blindingKey))
            .build()
        val coinSig = crypto.eddsaSign(p, Base32Crockford.decode(coin.coinPriv))
        return Request(
            coinBlindKeySecret = coin.blindingKey,
            coinPub = coin.coinPub,
            coinSig = Base32Crockford.encode(coinSig),
            denomPubHash = coin.denomPubHash,
            denomSig = coin.denomSig,
            refreshed = coin.coinSource === REFRESH,
        )
    }

}
