/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.serialization.Serializable
import net.taler.lib.common.Timestamp

/**
 * Structure that the exchange gives us in /keys.
 */
@Serializable
internal data class Keys(
    /**
     * List of offered denominations.
     */
    val denoms: List<Denomination>,

    /**
     * The exchange's master public key.
     */
    val master_public_key: String,

    /**
     * The list of auditors (partially) auditing the exchange.
     */
    val auditors: List<Auditor>,

    /**
     * Timestamp when this response was issued.
     */
    val list_issue_date: Timestamp,

    /**
     * List of revoked denominations.
     */
    val recoup: List<Recoup>?,

    /**
     * Short-lived signing keys used to sign online
     * responses.
     */
    val signkeys: List<SigningKey>,

    /**
     * Protocol version.
     */
    val version: String,
) {
    companion object {
        /**
         * Fetch an exchange's /keys with the given normalized base URL.
         */
        suspend fun fetch(httpClient: HttpClient, exchangeBaseUrl: String): Keys {
            return httpClient.get("${exchangeBaseUrl}keys")
        }
    }
}

/**
 * Structure of one exchange signing key in the /keys response.
 */
@Serializable
internal data class SigningKey(
    val stamp_start: Timestamp,
    val stamp_expire: Timestamp,
    val stamp_end: Timestamp,
    val key: String,
    val master_sig: String,
)

/**
 * Element of the payback list that the
 * exchange gives us in /keys.
 */
@Serializable
internal data class Recoup(
    /**
     * The hash of the denomination public key for which the payback is offered.
     */
    val h_denom_pub: String,
)
