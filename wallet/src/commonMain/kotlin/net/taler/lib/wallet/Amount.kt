/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import net.taler.lib.common.Amount
import net.taler.lib.crypto.CryptoImpl.Companion.toByteArray

internal fun Amount.toByteArray() = ByteArray(8 + 4 + 12).apply {
    value.toByteArray().copyInto(this, 0, 0, 8)
    fraction.toByteArray().copyInto(this, 8, 0, 4)
    currency.encodeToByteArray().copyInto(this, 12)
}
