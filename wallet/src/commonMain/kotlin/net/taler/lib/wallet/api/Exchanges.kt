/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.api

import net.taler.lib.wallet.exchange.ExchangeRecord

public data class ExchangeListItem(
    val exchangeBaseUrl: String,
    val currency: String,
    val paytoUris: List<String>,
) {
    internal companion object {
        fun fromExchangeRecord(exchange: ExchangeRecord): ExchangeListItem? {
            return if (exchange.details == null || exchange.wireInfo == null) null
            else ExchangeListItem(
                exchangeBaseUrl = exchange.baseUrl,
                currency = exchange.details.currency,
                paytoUris = exchange.wireInfo.accounts.map {
                    it.paytoUri
                },
            )
        }
    }
}

public data class GetExchangeTosResult(
    /**
     * Markdown version of the current ToS.
     */
    val tos: String,

    /**
     * Version tag of the current ToS.
     */
    val currentEtag: String,

    /**
     * Version tag of the last ToS that the user has accepted, if any.
     */
    val acceptedEtag: String? = null,
)
