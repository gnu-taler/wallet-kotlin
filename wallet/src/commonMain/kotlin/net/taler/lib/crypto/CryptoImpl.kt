/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.crypto

internal abstract class CryptoImpl : Crypto {

    companion object {
        fun Int.toByteArray(): ByteArray {
            val bytes = ByteArray(4)
            bytes[3] = (this and 0xFFFF).toByte()
            bytes[2] = ((this ushr 8) and 0xFFFF).toByte()
            bytes[1] = ((this ushr 16) and 0xFFFF).toByte()
            bytes[0] = ((this ushr 24) and 0xFFFF).toByte()
            return bytes
        }

        fun Long.toByteArray() = ByteArray(8).apply {
            var l = this@toByteArray
            for (i in 7 downTo 0) {
                this[i] = (l and 0xFF).toByte()
                l = l shr 8
            }
        }
    }

    override fun kdf(outputLength: Int, ikm: ByteArray, salt: ByteArray, info: ByteArray): ByteArray {
        return Kdf.kdf(outputLength, ikm, salt, info, { sha256(it) }, { sha512(it) })
    }

    override fun setupRefreshPlanchet(secretSeed: ByteArray, coinNumber: Int): FreshCoin {
        val info = "taler-coin-derivation".encodeToByteArray()
        val salt = coinNumber.toByteArray()
        val out = kdf(64, secretSeed, salt, info)
        val coinPrivateKey = out.copyOfRange(0, 32)
        val bks = out.copyOfRange(32, 64)
        return FreshCoin(eddsaGetPublic(coinPrivateKey), coinPrivateKey, bks)
    }

}
