/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.crypto

internal interface Crypto {
    fun sha256(input: ByteArray): ByteArray
    fun sha512(input: ByteArray): ByteArray
    fun getHashSha512State(): HashSha512State
    fun getRandomBytes(num: Int): ByteArray
    fun eddsaGetPublic(eddsaPrivateKey: ByteArray): ByteArray
    fun ecdheGetPublic(ecdhePrivateKey: ByteArray): ByteArray
    fun createEddsaKeyPair(): EddsaKeyPair
    fun createEcdheKeyPair(): EcdheKeyPair
    fun eddsaSign(msg: ByteArray, eddsaPrivateKey: ByteArray): ByteArray
    fun eddsaVerify(msg: ByteArray, sig: ByteArray, eddsaPub: ByteArray): Boolean
    fun keyExchangeEddsaEcdhe(eddsaPrivateKey: ByteArray, ecdhePublicKey: ByteArray): ByteArray
    fun keyExchangeEcdheEddsa(ecdhePrivateKey: ByteArray, eddsaPublicKey: ByteArray): ByteArray
    fun kdf(outputLength: Int, ikm: ByteArray, salt: ByteArray, info: ByteArray): ByteArray
    fun rsaBlind(hm: ByteArray, bks: ByteArray, rsaPubEnc: ByteArray): ByteArray
    fun rsaUnblind(sig: ByteArray, rsaPubEnc: ByteArray, bks: ByteArray): ByteArray
    fun rsaVerify(hm: ByteArray, rsaSig: ByteArray, rsaPubEnc: ByteArray): Boolean
    fun setupRefreshPlanchet(secretSeed: ByteArray, coinNumber: Int): FreshCoin
}

internal interface HashSha512State {
    fun update(data: ByteArray): HashSha512State
    fun final(): ByteArray
}
internal class EddsaKeyPair(val privateKey: ByteArray, val publicKey: ByteArray)
internal class EcdheKeyPair(val privateKey: ByteArray, val publicKey: ByteArray)
internal data class FreshCoin(val coinPublicKey: ByteArray, val coinPrivateKey: ByteArray, val bks: ByteArray) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false
        other as FreshCoin
        if (!coinPublicKey.contentEquals(other.coinPublicKey)) return false
        if (!coinPrivateKey.contentEquals(other.coinPrivateKey)) return false
        if (!bks.contentEquals(other.bks)) return false
        return true
    }

    override fun hashCode(): Int {
        var result = coinPublicKey.contentHashCode()
        result = 31 * result + coinPrivateKey.contentHashCode()
        result = 31 * result + bks.contentHashCode()
        return result
    }
}

internal expect object CryptoFactory {
    internal fun getCrypto(): Crypto
}

private val hexArray = arrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')

public fun ByteArray.toHexString(): String {
    val hexChars = CharArray(this.size * 2)
    for (j in this.indices) {
        val v = (this[j].toInt() and 0xFF)
        hexChars[j * 2] = hexArray[v ushr 4]
        hexChars[j * 2 + 1] = hexArray[v and 0x0F]
    }
    return hexChars.concatToString()
}
