/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp
import net.taler.lib.wallet.exchange.DenominationStatus.Unverified
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedGood

internal object Denominations {

    private val validStart = Timestamp.now()
    private val validExpireWithdraw =
        Timestamp(Timestamp.now().ms + 1000L * 60L * 60L * 24L * 365L)
    val denomination10 = DenominationRecord(
        denomPub = "020000X0X3G1EBB22XJ4HD6R8545R294TMCMA13ZRW7R101KJENFGTNTZSPGA0XP898FJEVHY4SJTC0SM264K0Y7Q6E24S35JSFZXD6VAJDJX8FCERBTNFV5DZR8V4GV7DAD062CPZBEVGNDEJQCTHVFJP84QWVPYJFNZSS3EJEK3WKJVG5EM3X2JPM1C97AB26VSZXWNYNC2CNJN7KG2001",
        denomPubHash = "7GB2YKDWKQ3DS2GA9XCVXVPMPJQA9M7Q0DFDHCX5M71J4E2PEHAJK3QF3KTJTWJA33KG0BX6XX0TTRMMZ8CEBM4GSE2N5FSV7GYRGH0",
        exchangeBaseUrl = "https://example.org",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        isOffered = true,
        isRevoked = false,
        masterSig = "N9ADAGY43VTDA000NMW9NFKY8HBTTNPEWP9W1A1ATCAFVA1A9F1MD7HAJ6M4QQ3SVYJXCV1S9Z1ZMKXP9YKD3PFGNK0VQD1ZZ502420",
        stampExpireDeposit = validExpireWithdraw,
        stampExpireLegal = validExpireWithdraw,
        stampExpireWithdraw = validExpireWithdraw,
        stampStart = validStart,
        status = Unverified,
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 10)
    )
    val denomination8 = denomination10.copy(
        denomPub = "020000X61XTJPQHC9Z3GEF8BD0YTPGH819V6YQDW7MZ74PH9GC2WZGH1YPBHB6F6AJG7C91G19KTSSP9GA64SFZ26T06QK6XEQNREEFV6EMVRCQRYXV3YVA5ANJQRFJVMQG3DQG6Q0ANQWRBZGZSX43MNJQ0NGZY2X1VHJ0351RC83RMPYV1JFSZ2J1JZ2MN5AJF6QMCBBJN0V5TF3EG2001",
        denomPubHash = "M496WWEBC8KN4MYB73V78F4DCXXMFWR2X33GWDA92X98MC04E120H9NVNKN70J3NP7ZZ91BE7CX65NYHQEG5EQK5Y78E7ZE3YV8E868",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 2000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 4000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 5000000, value = 0),
        masterSig = "GNWDZNQHFECF2SPVWZ5D0KV7YEGJ0J86ND815B512ZSBNKSCSN7PCKE5GJTXV0WZNS3AYTDJYER3W1HXSST4TMBMAR3FY3ETRNRS20G",
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 8)
    )
    val denomination5 = denomination10.copy(
        denomPub = "020000XFF9HD3GJXVA9ARQD76BW2G9K65F6CVDPWSRYVE5HY7EVVBK1PK1XX2HE2P3BA3A9MJT9ESY1XNKK7TTF8DRE33C22EHPNNBPPQC1D1MHEE3YJHNF8PG0W6DTE406R6VHCZ0VHEE5HNTEPWMAHJ5J0VVY1ESGAXWE1SGSY82FCQARWV45MNGYZMBN2M55CG3SQXJ8STRPHEM1G2001",
        denomPubHash = "4HKTYHDDCJXJW7Z215E3TFQWBRYKZA4VS41CZKBE8HH1T97FV4X9WSM62Q7WEZTZX3Y60T4Y8M4R0YYA3PVSEND1MZCJBTD4QZDMCJR",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        masterSig = "0TBJSTZTWMKBT71ET1B7RVFB1VT84E10G3SENPSMPRYTFBH07WSDS5VA9PW5T0EYEABDKGAYDRQ0XA62GYQXZ4SBV2CTHHFY0TFNC18",
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 5)
    )
    val denomination4 = denomination10.copy(
        denomPub = "020000Y3JVXB3XTC7JTK3CYEMSXHEX8956DN9B4Z6WB3J1H8D5Y3BVTY8EE5BC5JSRJKM0VAK6BXSHVRGQ6N43BF132DPNSJDG4CD8JA4A856HVCNFSNP0DY21TJYN8GJ36R1T0VKTVH2SRMT4QN1QQZC0VQ5ZV2EJJMCSVYVKV8MZC9NG5K9PGNQKBV64E34H1K9RSFEJE7306ZH07G2001",
        denomPubHash = "XRKJ5750TW2ZNQYGQ87TESYF2X942RBCG7EKXB8QKMFGB433EK3567SDSE9EFNBYTEH3PHPTK22V8XNSJ090DHYX8EW9BE1Q8JCZW7G",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 4000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 2000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        masterSig = "VFKWHPFNEKD36A0A0TWVTEN98P4TCVBTRMJFJF1E1Q2578J9DXAY53Y7Q2D3BX01WKPM7QBCVF9WBMB4GTXVFP08QPZP086RV9PAP2G",
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 4)
    )
    val denomination2 = denomination10.copy(
        denomPub = "020000XB8Y8WVSBM0WBY6NMS0MTNN71PEA6KGKTV5FF27RRR8V1V0GPWYAFDMD2XT6E265QPNH079KM25ZZ97NGFRDVHW0Y7JPWA9C8MJY8DB7APYBRMD9XYA0N1569VFW2NPP4FGQJ865RVE94F35NSG0M4W80CG6WXXWW1ERRM7F2AGRZE9FS049183ANEDFB7QN4H62GDWS7039GG2001",
        denomPubHash = "CTH34KGWJKQ3C264DQJCRPX97G5SWGWQQ8EMBXVH4DKGTPJ78ZA5CWZB9SDT538Z6S118VQ3RNX3CVC1YXEFN0PXR0D896MDNCGZW3G",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 4000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 2000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        masterSig = "BTZYYEYBZK1EJJ47MQHEY7Q8ARBK4PT0N92TYEEPEZ54P0NTN6FT50AGKCCQCWQ8J74D8MTZFAX3PRDWSH30JPVREAQKGVGEVYXN00G",
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 2)
    )
    val denomination1 = denomination10.copy(
        denomPub = "020000XBEVGYETENRYN30AMTT006NPFG5QNHHJXESR9YRZ0J1Y0V00ASMSK7SGKBZJW1GKW05AGNXZDGHAT8RP5M87H2QZTCZHNWW0W4SFWSHWEMRK57DQ3Z7EYQ3XMFX8E2QNZNT9TB4J0MMDP833Y0K7RCQ3X5A584ZBQ9M0T03KTF1QTS2Z7J0BRCEVMK7CCM5WYCAVDPP44Z23HG2001",
        denomPubHash = "KHRCTNBZHY653V3WZBNMTGGM3MSS471EZ4F6X32HJMN17A47WBBM5WHCRNK8F27KB6Q45YMEN832BKYNKBK1GCRXKDP0XTYC3CYTRWR",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 2000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 2000000, value = 0),
        masterSig = "X029BKSB9PRKE831VDV1X3ND441E0NNQCDJ2S9NKPG7F17R0Y6QCGKWCSHCHE96NW5XS13RK880H90ZSFY7HNJ17F4NSR1EM5FR2M10",
        value = Amount(currency = "TESTKUDOS", fraction = 0, value = 1)
    )
    val denomination0d1 = denomination10.copy(
        denomPub = "020000YVVE91KXE9S8JV5P4PWZJWBJD39Y6S00FA3CV6RP77A8PTKJWNE3W0W8ZHDR3CSKA17FT56PMC97RWV3RNG753B1WQYXEWNJA76GD5T2PA33BN08CQ07KP4M2K9R6A3N9VD6W8D3DK55W18Y7TBKAHCJBQ3AZ50VHSF1ZPM2XVJ238SKK1379WNHMK4VDJQ35H20QSF3GPWPKG2001",
        denomPubHash = "C26V15X3BESS1CZCSP4BNSRJ8BK8DSGFHNB0WG1GZ6ZF6FR37WGSVEQ85A61X6Z103P8MY7XGQZ60VAX78V3E5GERWJTJAP0Q5QB7A8",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 3000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        masterSig = "JSJXWJXN4532C07CBH4DZZ6YC5S3TPH3H8FG8RMGX0Z647GX2NYEK02NRN3C4GDS9Q0ZT6QE7T8EGYGEF9RZ9FCV65TZWC1ZP83DT1R",
        value = Amount(currency = "TESTKUDOS", fraction = 10000000, value = 0)
    )
    val denomination0d01 = denomination10.copy(
        denomPub = "020000X67SMNYMCR1HFZW4KEATXGXRA983JE5VW1JE4108XB7Z40BTJC1MV59Y9K4Y35E3MPPF73BJQA3KVT0FBT89R6ZYNZ77TSC5DMFV5E55DT4JB4S9K4C2V7GM8Z8QZ0KMCH0YK4PAX1WSKCEQFNRVKD3VH9WTVQ0CNV7Z1JVRHBKCSZNX62TRQ6JRZ05JEANT3C41SQ6MKSQZKG2001",
        denomPubHash = "VS0E7ABRZ9NZMTDAHVKQ4QH1S1Q2PYWXFXKF0P84VSHCDBM4S6QK1G1D495TABN4AXVX049P7JESNRRQVHW37BNER4XKZSQT3XA61DG",
        feeDeposit = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeRefresh = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeRefund = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        feeWithdraw = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0),
        masterSig = "CF3638K8QBG91D66JZRRGWV8J53A0ZGBYJDMKYK293DNAA2ASM1M346C0YG9V7HWH8E2A7FPVR0HH2QE7DHD9GW0EN19VSER4S5F23R",
        status = VerifiedGood,
        value = Amount(currency = "TESTKUDOS", fraction = 1000000, value = 0)
    )

}
