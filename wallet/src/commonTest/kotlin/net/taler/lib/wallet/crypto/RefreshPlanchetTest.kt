/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.crypto

import net.taler.lib.crypto.CryptoFactory
import net.taler.lib.crypto.FreshCoin
import net.taler.lib.crypto.Base32Crockford
import kotlin.test.Test
import kotlin.test.assertEquals

class RefreshPlanchetTest {

    private val crypto = CryptoFactory.getCrypto()

    private val vectors = listOf(
        TestVector(
            "A9M953G6CJWBD21S82784Q3ZFEZYYWWKB9M6K4WQTTJZ8VTHPTTSHN8W3RMJQCCEKAFM04QD1PAQEN80DR6K0MNKYP404KMYW494D6R",
            27,
            FreshCoin(
                Base32Crockford.decode("JR124F1W9Q984Y01Y323CN88MVCJN7D8BJHYHCST5NWJTNCJMCQG"),
                Base32Crockford.decode("YQS09P0FGBWS2FYY4NE5EDB93MKAYATNT8GZ33GSM34KF9PW6YWG"),
                Base32Crockford.decode("KMWCX1W2HDKQM55ZRDZ630JE75YNDP8S8X8J02F9VSVZMNBWQ70G")
            )
        ),
        TestVector(
            "MRQ6KTFKJ5JMQS8QV5Z189W110V7F6BJJQMDHVKY17HDMTY7MYSB1MKRJY0ZGF0SC1TSZQNT7NJPJVJE17VBSBH05MJ26SQVPXCM9R8",
            65,
            FreshCoin(
                Base32Crockford.decode("W6BVA4JQKDJ294HFBDQGZK0VGGKRRXG820WWPJ1Y2TJ9SQYEXZWG"),
                Base32Crockford.decode("FKH9KW7F73V8F4KEYME899RTKED2DVRM3A74MJJ5YT3P3CCGFS60"),
                Base32Crockford.decode("BNDT4TPMV2R7HJVJAA8ZPF9J3Q0V1ZHY1JQ76723CBBQ99RX0GR0")
            )
        ),
        TestVector(
            "SCHMBND9ZMHDHTWQTSF1M41W4WPF9GM3FR5PJHC5JTZM25XEEC7CWC5SEN8FEWM54MSH27B9DJG91AKZ2YZ7ZMERG14QTBYJC02K21G",
            89,
            FreshCoin(
                Base32Crockford.decode("J75FF26KDZPBQW960SBG81W24BAY0YA04ZJ6PKQ1V0NQ7K62XNC0"),
                Base32Crockford.decode("QS7BMWNG7SAEX7Z4H6QF66J5Q8GAX7JWHD4JTM9A656N43RH66C0"),
                Base32Crockford.decode("JTNY89C0Y69X0NGMK2KB2JGGQZXX41GASPBN1YRS7KKV0MTP84CG")
            )
        ),
        TestVector(
            "S8DC2D6N1TW1TJQFRVDM5Q2HZ9G66MWVS4651GB2ST49TPFK2KWCRSADH51YNCMNGWK4JFT2SKQNWVA17XAR7EC18Z0X214PM1RF2MG",
            37,
            FreshCoin(
                Base32Crockford.decode("QWP8560H613GSNR2A1NGVQ023V2BP8ZYBVJJ0E2Y66ZGSHCX92K0"),
                Base32Crockford.decode("XF58PHSGFDC30CBWQ55KDFWBDSZJMQ3AA6DF4V9SQ4E2DEFBS7N0"),
                Base32Crockford.decode("KQT2K5HDKVVBHD188XS0KFBM63B4MAWM42HABT9FH806GME1QXHG")
            )
        ),
        TestVector(
            "THX83Y6G26CYMF186V6GPP5A4DQ6624D9EGCA6R7DVW6T32P6E6Q0TDW0S9MJSRG84SR2AMK1KKS6D627EJ02XSNH0BXDNWD1W35BWR",
            31,
            FreshCoin(
                Base32Crockford.decode("QWCD3DMV7NX87SPHYFWE3STBZQ02Q1NMXJ8GQGAA6WZ4GF7B3C80"),
                Base32Crockford.decode("7J26RGBJFRGAYX8XDZZZ8PE9PCW3DH5KFA7564WZ6F5ADC159KB0"),
                Base32Crockford.decode("M0A1EPG9WFB02Y7R6YYE6V6Q8DRJ466XCD6N99YFDAGN1QVA62NG")
            )
        ),
        TestVector(
            "D949HYZA6MWNKBKGTRRP1SGFA4X7AZK7HG5XX9YBVMWA05B884WAYY7MRDQNQYMRGA7AE01V6EEWZDF9JXMQKSYG0G1FVSZG4096JV8",
            26,
            FreshCoin(
                Base32Crockford.decode("BXRTK601B5NRJV9BAE88SAVPSMA20ZWDH5P0T6WFY7BSM3SZD930"),
                Base32Crockford.decode("5N4WJWFR1J1SDNJGC8G9AV3ET4MPF6TGA4S1PJNQRJ5BRDHEEJN0"),
                Base32Crockford.decode("P7XFE4W0H444XBYPRHB9VBTHKAZHNJE4E0R7P4D5FT90W3JJRE20")
            )
        ),
        TestVector(
            "7CXEEDABFD2FM802314FE91J2DS7ZHCHDGX27SJHJWWJWJZ1NGZBNZJ8T55W1P9ZEBPVYCHDNKQWRG68K3EJD069723857R06XDYE00",
            2147483647,
            FreshCoin(
                Base32Crockford.decode("QCAK6CA31KJ7HX4V3QEFK9CENT3E6DVE76M4VY9ZQ030CM8TZQJG"),
                Base32Crockford.decode("2V7HAFK2ATBB9MDWRR8P9PSPXBWMGFXW5G75WVGYCRZB0G5PDG9G"),
                Base32Crockford.decode("6C02KA6496FG5X0SDSWTS4JJHH87GYNSW9FZE6S34AWG5RJGPWQ0")
            )
        ),
        TestVector(
            "9F65N3N1BZW7TT6EB4X2GCDC5BN3J2R33R7H0NWPZENN6B4Y41XYTVC9SA5NP0E4WQFQEJ63WNC5R0C8KBMKDA5JCXEKEGJYVMTR7VG",
            0,
            FreshCoin(
                Base32Crockford.decode("S6RM093DHGG882T39KWC48807CFC36SRGGX54DBB1Z6NDAMT875G"),
                Base32Crockford.decode("2DY3KVKA978S85ERF6QYF73Q95KQ0YVE89KKQ8JYR0FX161PV1S0"),
                Base32Crockford.decode("PCY47EC1APBKBG9HWQCTJN6FWNJPGGC6XDJTYWBHTJ9A952AGCK0")
            )
        )
    )

    @Test
    fun testRefreshPlanchets() {
        for (v in vectors) {
            val freshCoin = crypto.setupRefreshPlanchet(Base32Crockford.decode(v.seed), v.coinNumber)
            assertEquals(v.freshCoin, freshCoin)
        }
    }

    private class TestVector(val seed: String, val coinNumber: Int, val freshCoin: FreshCoin)

}
