/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import net.taler.lib.common.Timestamp
import net.taler.lib.crypto.Base32Crockford
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class TimestampTest {

    companion object {
        fun getRandomTimestamp() = Timestamp(Random.nextLong(0, 9007199254740991))
    }

    @Test
    fun testRoundedToByteArray() {
        val vectors = listOf<Pair<Long, String>>(
            Pair(0, "0000000000000"),
            Pair(23, "0000000000000"),
            Pair(3349, "000000005Q3C0"),
            Pair(61227, "00000003MB4M0"),
            Pair(143879, "00000008GR0W0"),
            Pair(8879237, "000000GH7B4W0"),
            Pair(16058145, "000000XX46H80"),
            Pair(270909464, "00000FRKDX4M0"),
            Pair(5500325225, "0000A054XBSM0"),
            Pair(52631835363, "0002ZQJDTGYC0"),
            Pair(567067373675, "00107FN9AKAM0"),
            Pair(1036693403335, "001TXQFY0VEC0"),
            Pair(88636710366804, "04XED7JKDJSR0"),
            Pair(852207301364437, "1F9TC1M0SEJG0"),
            Pair(8312646819781097, "EDE78FC4AEXM0"),
            Pair(7473472692572260, "CYVHQMAQAR7G0"),
            Pair(1148188526507363, "1ZQJYRD9M40C0"),
            Pair(5418115526173127, "9CRG6QASJ80M0"),
            Pair(4046218176532046, "70KGVZK7XCPG0"),
            Pair(2421361923399585, "46D6FNS4VAFW0"),
            Pair(7305555710693483, "CNH8RDJYNV1M0"),
            Pair(2857858080018042, "4YMJDJ1XYFM80"),
            Pair(2037218281967033, "3H2TEEYTJCCW0"),
            Pair(7912348432268295, "DQ74XYJCEFXG0"),
            Pair(6416777738213721, "B46FJPQRT81M0"),
            Pair(6914097778740296, "BZSWYK0W3NTG0"),
            Pair(7090360690428000, "C9K0AYTABAVG0"),
            Pair(1998560202566445, "3EY4ZTJR5QER0"),
            Pair(7896179665141956, "DPADV4EQCHKM0"),
            Pair(6266851558322330, "AVW58JG1WB880"),
            Pair(1878397422799871, "388PH07WY68W0"),
            Pair(3767372253320333, "6H46A6MZSMS00"),
            Pair(8065344266359580, "DZPXQR6KHZ5W0"),
            Pair(7947440620360995, "DS5FP8RA25H00"),
            Pair(3414000286898485, "5XGFEB1VMC880"),
            Pair(9007199254740991, "FKZZZZZZY3EG0")
        ) // TODO add more test vectors beyond 9007199254740991 (typescript max of wallet-core)
        for (v in vectors) {
            val encodedBytes = Base32Crockford.encode(Timestamp(v.first).roundedToByteArray())
            assertEquals(v.second, encodedBytes)
        }
    }

}
