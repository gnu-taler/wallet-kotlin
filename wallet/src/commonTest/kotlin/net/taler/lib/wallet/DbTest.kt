/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import net.taler.lib.common.Timestamp
import net.taler.lib.wallet.exchange.DenominationStatus.Unverified
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedGood
import net.taler.lib.wallet.exchange.Denominations.denomination10
import net.taler.lib.wallet.exchange.Denominations.denomination5
import net.taler.lib.wallet.exchange.ExchangeRecord
import net.taler.lib.wallet.exchange.ExchangeUpdateReason.Initial
import net.taler.lib.wallet.exchange.ExchangeUpdateStatus.FetchKeys
import kotlin.test.Test
import kotlin.test.assertEquals

class DbTest {

    private val dbFactory = DbFactory()

    private val exchange1 = ExchangeRecord(
        baseUrl = "https://example1.org/",
        timestampAdded = Timestamp.now(),
        updateStatus = FetchKeys,
        updateStarted = Timestamp.now(),
        updateReason = Initial
    )
    private val exchange2 = ExchangeRecord(
        baseUrl = "https://example2.org/",
        timestampAdded = Timestamp.now(),
        updateStatus = FetchKeys,
        updateStarted = Timestamp.now(),
        updateReason = Initial
    )

    @Test
    fun testExchanges() = runCoroutine {
        val db = dbFactory.openDb()
        var exchanges = db.listExchanges()
        assertEquals(0, exchanges.size)

        db.put(exchange1)
        exchanges = db.listExchanges()
        assertEquals(1, exchanges.size)
        assertEquals(exchange1, exchanges[0])

        db.put(exchange2)
        exchanges = db.listExchanges()
        assertEquals(2, exchanges.size)
        assertEquals(exchange1, db.getExchangeByBaseUrl(exchange1.baseUrl))
        assertEquals(exchange2, db.getExchangeByBaseUrl(exchange2.baseUrl))

        db.deleteExchangeByBaseUrl(exchange1.baseUrl)
        exchanges = db.listExchanges()
        assertEquals(1, exchanges.size)
        assertEquals(exchange2, exchanges[0])
    }

    @Test
    fun testDenominations() = runCoroutine {
        val db = dbFactory.openDb()
        val url = denomination10.exchangeBaseUrl

        // no denominations should be in DB
        var denominations = db.getDenominationsByBaseUrl(url)
        assertEquals(0, denominations.size)

        // add a denomination and check that it is really there
        db.put(denomination10)
        denominations = db.getDenominationsByBaseUrl(url)
        assertEquals(1, denominations.size)
        assertEquals(denomination10, denominations[0])

        // modify existing denomination and check that it gets updated
        assertEquals(Unverified, denomination10.status)
        val newDenomination = denomination10.copy(status = VerifiedGood)
        db.put(newDenomination)
        denominations = db.getDenominationsByBaseUrl(url)
        assertEquals(1, denominations.size)
        assertEquals(newDenomination, denominations[0])

        // add one more denomination
        db.put(denomination5)
        denominations = db.getDenominationsByBaseUrl(url)
        assertEquals(2, denominations.size)
    }

}
