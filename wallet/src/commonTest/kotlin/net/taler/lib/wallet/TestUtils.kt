/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.MockRequestHandler
import io.ktor.client.engine.mock.respond
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.http.ContentType.Application
import io.ktor.http.Url
import io.ktor.http.fullPath
import io.ktor.http.headersOf
import io.ktor.http.hostWithPort
import kotlinx.coroutines.CoroutineScope
import kotlin.native.concurrent.ThreadLocal

enum class PlatformTarget {
    ANDROID,
    JS,
    NATIVE,
}

/**
 * Workaround to use suspending functions in unit tests
 */
expect fun runCoroutine(block: suspend (scope: CoroutineScope) -> Unit)

expect fun getPlatformTarget(): PlatformTarget

@ThreadLocal
object ChangeableRequestHandler {
    var myHandler: MockRequestHandler = { _ ->
        error("No HttpClient request handler added. Use e.g. giveJsonResponse()")
    }
}

fun getMockHttpClient(): HttpClient = HttpClient(MockEngine) {
    install(JsonFeature) {
        serializer = KotlinxSerializer(getJson())
    }
    engine {
        addHandler { ChangeableRequestHandler.myHandler(this, it) }
    }
}

fun giveJsonResponse(url: String, jsonProducer: () -> String) {
    ChangeableRequestHandler.myHandler = { request ->
        if (request.url.fullUrl == url) {
            val headers = headersOf("Content-Type" to listOf(Application.Json.toString()))
            respond(jsonProducer(), headers = headers)
        } else {
            error("Unexpected URL: ${request.url.fullUrl}")
        }
    }
}

private val Url.hostWithPortIfRequired: String get() = if (port == protocol.defaultPort) host else hostWithPort
private val Url.fullUrl: String get() = "${protocol.name}://$hostWithPortIfRequired$fullPath"
