/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp
import net.taler.lib.wallet.getMockHttpClient
import net.taler.lib.wallet.giveJsonResponse
import net.taler.lib.wallet.runCoroutine
import kotlin.test.Test
import kotlin.test.assertEquals

class KeysTest {

    private val httpClient = getMockHttpClient()

    @Test
    fun testFetchKeys() {
        val expectedKeys = Keys(
            denoms = listOf(
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:5"),
                    denom_pub = "040000Z9TH9RPTA1BXF6Z89HM7JGXTPD5G8NNBWQWF7RWQGNAATN84QBWME1TGSWZ79WPQ62S2W2VHG2XBH66JDJ0KM8Q2FQ3FGBZQGNJVFNA9F66E6S3P36KTMWMKWDXWM9EX1YHSHQ841AHRR8JVDY96CZ13AJF6JW95K59AE8CSTH5ZS9NVS0102X92GK8JW2QX2S4EE25QNHK6XMXH3944QMXPFS7SFCMV623BM62VNPVX8JM424YXPJ09TXZAH2CF3QM5HDVRSTDRDGVBF6KZVRFM852TMVMYPVGFA9YQF6HWNJ8H5VCQ3Z9WWNMQ3T76X4F1P6W2J266K8B3W9HKW2WJNK3XHRAVC4GCF07TC0ZNAT0EDAAKV429YAXWSK952BPTY98GVP5XZQG2SE0Q5CF3PV04002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "2SDD44VVBD52XEV0A9R878BC60J51VKK0H5ZS6CPJ7Z738A8V4KPXCF70KFZAY2567400C2GEWNNVXF6PYD7HKX3D2M63WCNPJSE010"
                ),
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:2"),
                    denom_pub = "040000XV91V0M7H906Y7R371YX2XAK1V5B2TRFD8ZM9WYJ495TP08NCVEDNFXS2KZBJR4808VZ52PNNQSYVQ2T3J7867MZQY1QZ9N8YQWQWCKSYAY8A07E5SYAK0G0KRTCN5VZ7JXE2YCNT7Q3RT9TGAZBSK5V1ZRRK6HX4C1YFKPWWP4TBVJ8DJMS43WKR4CR4S9T02YXVGR6GSDMR7GHBD89JHCEQ8V2K58Y5XVDGRRQYNBG9Q5XWDMV7GKN24JCPCEKSZZP5XYPXYJX2Z2JZ179M9FQV0PEWFJ4DP7AP14XE54FH97YP9398KA31ECVDY7PHMKMZ6E79Z9FSCXH3WSCXMHBWFGWPRG5ZG1P6HR71VKH4F0Y998JNE4G40JH2VSXP3035AR7HAMJGB56CYHH60EWD904002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "WX1RDTW1W1K7FPFQQ9MCJ8V5CJP8ZX2SGHQG2YS34BY8AMSR3YQ92HE2HT1JMP4W06J63RZ0BR2MKDBX54GV6QKD4R00N9HXN2S283R"
                ),
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:1"),
                    denom_pub = "040000XCBJE9TDDZATYSDR51D0DKMY5NW8FMJ8YQ1Y4F40SPPTKFMD3FWH38NSQZ1YB621TCFH5RBN5J3SFR5SG4789G27FA90E605GG9AXYTXXPJ9HYPAVAVS6V4XCSC17HKX2M2NSX5D0PPETDGKQD04G498VS36YY4WTB5SYG4SV9MKPVZ5WG2WNP3MA77TFZSHK5HBHZBEW0S1TRKGSCDNBRHYB240M84YM1Y7EJ7BXKJK4GRR1GS16DJ2RA1YEQ1AAXH0GP6RRAEJE8D2JFSH05P3KR1GB97NMX6VD8DCAX45416F888EYQR4M6R820FJVZ6FYV9CCMZ3M10B64N6G4QFNKFNAV2ENPVVG4A3R0AAA6STJ7E5Z05GEKW35SHM14HY9CEGM7D1ZEKHZJYA9P6WH504002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "65G9FWQPA4YKJEM7D37079D4MY81D47KD1280RG7BRH85XZQ2N13FJPV9N8AEASK9CTGNX1HKX0GTRBJ5C49H4YRY0E4CYVPNH06W18"
                ),
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:10"),
                    denom_pub = "040000XZDZK4BPPPXR7MYKK2AF4WF95EH3VF8WEX7WDX4HEWXSB5XX10N4V5RHFSK0TSBKNC9CRNVGK3WJ42S3Z9SB4Q3M4DQQ7DKCGKED6WBKENHT8JX51K1VR5JKCMAFBNM6DR5MNRGKFC2MDRQ0Y4BCXHKEMRD65C6JPBKYW9HJH66FGT22WMBV0AV7P60CKR13MQG6FKWW3TZW3XXHVY2VX9MJN6VQFPS6NQGGTNXZV2SK2X5MJAJME7RN9BNZ5ZBTW1CYMVCHBSVGBFPRC68W78PW44VP402VD12KG2AWKPD4DRBAA85HM1DN1KADYQ498QHYGEB3T3HH990HRV8PSNBGYCHB87JTVYMJ4N2PSP2FCX0H6FRTW1FQY05EB7D8BFXM95DNRCHVQSHBZ9RP7NZFA304002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "M423J7CJPACTPBYCFVR87B44JAJKAB2ME8C263WGHJSA8V8444SX428MVC9NF4GD08CKS9HY0WB4B8SEZ3HJFWKXNSH80RBJXQC822G"
                ),
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:0.1"),
                    denom_pub = "040000YKYFF6GX979JS10MEZ16BQ7TT6XBTE0TBX6VJ9KSG7K4D91SWJVDETNKQJXAFK9SAB3S31FZFA0Y0X22TKRXKCT7Z4GZCCRJJ12T1A5M4DWRTZDFRD3FE495NXHVPFM96KXMKH1HABTDDFZN0NWQ3NBJ6GNXD40NJ95E955X948JHBDJZWM3TEAK4XFJX8056XFDHVNXSF4VN14RR1WD1J5K7JPS61SKRNF3HT6NZA823PZW2KPV2KVBMMP615A922ZNJGVQDTW5TYWTK5DCBGG1YEKQRYF39NX9X722FZK98BTMHHH6WZFCKBT096G9BKSHSJW3VE8KKPCN8XGWYYPD3158HRKSA28BJQ9XJVVB6FDCGZ154WWGGSGW82BDYDH7ZHJBMS046AG0ND4ZCVR2JQ04002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "RKZKGR0KYKY0VZ26339DZKV8EZJ2HRRQMFSAJDHBG3YHEQNZFHKM8WPYCH9WHXTWBB10GQN9QJKFDJJF2H6D5FT801GF87G153PTJ18"
                ),
                Denomination(
                    value = Amount.fromJSONString("TESTKUDOS:1000"),
                    denom_pub = "040000Y9PBY1HPBDD4KSK9PBA86TRY13JQ4284T4E0G4SEREQ8YM88PZHKW1ACKT1RTWVTBXX83G54NFVYRJQX9PTDXDJ1CXSS42G8NYMW97NA6NNNASV69W1JX39NTS1NVKXPW4WMBASATSNBTXHRT92FFN2NAJFGK876BNN3TPTH57C76ADAQV43VFF7CYAWWNYZAYGQQ1XY1NK34FJD778VFGYCZ1G9J8XPNB92ZKJBZEZKSNBRNH27GM5A736AFSGP7B4JSCGD0F4FMD1PDVB26MM9ZK8C1TDKXQ5DJ09AQQ55P7Q3A133ASPGBH6SCJTJYH8C9A451B0SP4GDX2ZFRSX5FP93PY4VKEB36KCAQ5E2MRZNWFB6T0JK0W7Z7NXP5FW2VQ4PNV7B2NQ3WFMCVRSDSV04002",
                    fee_withdraw = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_deposit = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refresh = Amount.fromJSONString("TESTKUDOS:0.01"),
                    fee_refund = Amount.fromJSONString("TESTKUDOS:0.01"),
                    stamp_start = Timestamp(1582484881000),
                    stamp_expire_withdraw = Timestamp(1677092881000),
                    stamp_expire_legal = Timestamp(1897844881000),
                    stamp_expire_deposit = Timestamp(1740164881000),
                    master_sig = "FJPQKECRKVQSTB9Y983KDGD65Z1JHQKNSCC6YPMBN3Z4VW0AGC5MQM9BPB0YYD1SCMETPD6QB4X80HWE0ZDGWNZB1KND5TP567T4G3G"
                )
            ),
            master_public_key = "DY95EXAHQ2BKM2WK9YHZHYG1R7PPMMJPY14FNGP662DAKE35AKQG",
            auditors = emptyList(),
            list_issue_date = Timestamp(1592161681000),
            recoup = emptyList(),
            signkeys = listOf(
                SigningKey(
                    stamp_start = Timestamp(1592161681000),
                    stamp_expire = Timestamp(1594580881000),
                    stamp_end = Timestamp(1655233681000),
                    key = "0FMRBH8FZYYMSQ2RHTYYGK2BV33JVSW6MTYCV7Y833GVNXFDYK10",
                    master_sig = "368HV41Z4FNDXQ7EP6TNAMBSKP44PJAZW27EPH7XJNVG2A6HZQM7ZPMCB6B30HG50S95YD1K2BAJVPEYMGF2DR7EEY0NFBQZZ1B8P1G"
                ),
                SigningKey(
                    stamp_start = Timestamp(1594580881000),
                    stamp_expire = Timestamp(1597000081000),
                    stamp_end = Timestamp(1657652881000),
                    key = "XMNYM62DQW0XDQACCYDMFTM5GY7SZST60NH7XS9GY18H8Q9N7QN0",
                    master_sig = "4HRJN36VVJ87ZC2HZXP7QDSZN30YQE8FCNWZS3RCA1HGNY9Q0JPMVJZ79RDHKS4GYXV29PM27DGCN0VB0BCZFF2FC6FMF3A6ZNKC238"
                )
            ),
            version = "7:0:0"
        )

        giveJsonResponse("https://exchange.test.taler.net/keys") {
            """{
              "version": "7:0:0",
              "master_public_key": "DY95EXAHQ2BKM2WK9YHZHYG1R7PPMMJPY14FNGP662DAKE35AKQG",
              "reserve_closing_delay": {
                "d_ms": 2419200000
              },
              "signkeys": [
                {
                  "stamp_start": {
                    "t_ms": 1592161681000
                  },
                  "stamp_expire": {
                    "t_ms": 1594580881000
                  },
                  "stamp_end": {
                    "t_ms": 1655233681000
                  },
                  "master_sig": "368HV41Z4FNDXQ7EP6TNAMBSKP44PJAZW27EPH7XJNVG2A6HZQM7ZPMCB6B30HG50S95YD1K2BAJVPEYMGF2DR7EEY0NFBQZZ1B8P1G",
                  "key": "0FMRBH8FZYYMSQ2RHTYYGK2BV33JVSW6MTYCV7Y833GVNXFDYK10"
                },
                {
                  "stamp_start": {
                    "t_ms": 1594580881000
                  },
                  "stamp_expire": {
                    "t_ms": 1597000081000
                  },
                  "stamp_end": {
                    "t_ms": 1657652881000
                  },
                  "master_sig": "4HRJN36VVJ87ZC2HZXP7QDSZN30YQE8FCNWZS3RCA1HGNY9Q0JPMVJZ79RDHKS4GYXV29PM27DGCN0VB0BCZFF2FC6FMF3A6ZNKC238",
                  "key": "XMNYM62DQW0XDQACCYDMFTM5GY7SZST60NH7XS9GY18H8Q9N7QN0"
                }
              ],
              "recoup": [],
              "denoms": [
                {
                  "master_sig": "2SDD44VVBD52XEV0A9R878BC60J51VKK0H5ZS6CPJ7Z738A8V4KPXCF70KFZAY2567400C2GEWNNVXF6PYD7HKX3D2M63WCNPJSE010",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000Z9TH9RPTA1BXF6Z89HM7JGXTPD5G8NNBWQWF7RWQGNAATN84QBWME1TGSWZ79WPQ62S2W2VHG2XBH66JDJ0KM8Q2FQ3FGBZQGNJVFNA9F66E6S3P36KTMWMKWDXWM9EX1YHSHQ841AHRR8JVDY96CZ13AJF6JW95K59AE8CSTH5ZS9NVS0102X92GK8JW2QX2S4EE25QNHK6XMXH3944QMXPFS7SFCMV623BM62VNPVX8JM424YXPJ09TXZAH2CF3QM5HDVRSTDRDGVBF6KZVRFM852TMVMYPVGFA9YQF6HWNJ8H5VCQ3Z9WWNMQ3T76X4F1P6W2J266K8B3W9HKW2WJNK3XHRAVC4GCF07TC0ZNAT0EDAAKV429YAXWSK952BPTY98GVP5XZQG2SE0Q5CF3PV04002",
                  "value": "TESTKUDOS:5",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                },
                {
                  "master_sig": "WX1RDTW1W1K7FPFQQ9MCJ8V5CJP8ZX2SGHQG2YS34BY8AMSR3YQ92HE2HT1JMP4W06J63RZ0BR2MKDBX54GV6QKD4R00N9HXN2S283R",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000XV91V0M7H906Y7R371YX2XAK1V5B2TRFD8ZM9WYJ495TP08NCVEDNFXS2KZBJR4808VZ52PNNQSYVQ2T3J7867MZQY1QZ9N8YQWQWCKSYAY8A07E5SYAK0G0KRTCN5VZ7JXE2YCNT7Q3RT9TGAZBSK5V1ZRRK6HX4C1YFKPWWP4TBVJ8DJMS43WKR4CR4S9T02YXVGR6GSDMR7GHBD89JHCEQ8V2K58Y5XVDGRRQYNBG9Q5XWDMV7GKN24JCPCEKSZZP5XYPXYJX2Z2JZ179M9FQV0PEWFJ4DP7AP14XE54FH97YP9398KA31ECVDY7PHMKMZ6E79Z9FSCXH3WSCXMHBWFGWPRG5ZG1P6HR71VKH4F0Y998JNE4G40JH2VSXP3035AR7HAMJGB56CYHH60EWD904002",
                  "value": "TESTKUDOS:2",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                },
                {
                  "master_sig": "65G9FWQPA4YKJEM7D37079D4MY81D47KD1280RG7BRH85XZQ2N13FJPV9N8AEASK9CTGNX1HKX0GTRBJ5C49H4YRY0E4CYVPNH06W18",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000XCBJE9TDDZATYSDR51D0DKMY5NW8FMJ8YQ1Y4F40SPPTKFMD3FWH38NSQZ1YB621TCFH5RBN5J3SFR5SG4789G27FA90E605GG9AXYTXXPJ9HYPAVAVS6V4XCSC17HKX2M2NSX5D0PPETDGKQD04G498VS36YY4WTB5SYG4SV9MKPVZ5WG2WNP3MA77TFZSHK5HBHZBEW0S1TRKGSCDNBRHYB240M84YM1Y7EJ7BXKJK4GRR1GS16DJ2RA1YEQ1AAXH0GP6RRAEJE8D2JFSH05P3KR1GB97NMX6VD8DCAX45416F888EYQR4M6R820FJVZ6FYV9CCMZ3M10B64N6G4QFNKFNAV2ENPVVG4A3R0AAA6STJ7E5Z05GEKW35SHM14HY9CEGM7D1ZEKHZJYA9P6WH504002",
                  "value": "TESTKUDOS:1",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                },
                {
                  "master_sig": "M423J7CJPACTPBYCFVR87B44JAJKAB2ME8C263WGHJSA8V8444SX428MVC9NF4GD08CKS9HY0WB4B8SEZ3HJFWKXNSH80RBJXQC822G",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000XZDZK4BPPPXR7MYKK2AF4WF95EH3VF8WEX7WDX4HEWXSB5XX10N4V5RHFSK0TSBKNC9CRNVGK3WJ42S3Z9SB4Q3M4DQQ7DKCGKED6WBKENHT8JX51K1VR5JKCMAFBNM6DR5MNRGKFC2MDRQ0Y4BCXHKEMRD65C6JPBKYW9HJH66FGT22WMBV0AV7P60CKR13MQG6FKWW3TZW3XXHVY2VX9MJN6VQFPS6NQGGTNXZV2SK2X5MJAJME7RN9BNZ5ZBTW1CYMVCHBSVGBFPRC68W78PW44VP402VD12KG2AWKPD4DRBAA85HM1DN1KADYQ498QHYGEB3T3HH990HRV8PSNBGYCHB87JTVYMJ4N2PSP2FCX0H6FRTW1FQY05EB7D8BFXM95DNRCHVQSHBZ9RP7NZFA304002",
                  "value": "TESTKUDOS:10",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                },
                {
                  "master_sig": "RKZKGR0KYKY0VZ26339DZKV8EZJ2HRRQMFSAJDHBG3YHEQNZFHKM8WPYCH9WHXTWBB10GQN9QJKFDJJF2H6D5FT801GF87G153PTJ18",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000YKYFF6GX979JS10MEZ16BQ7TT6XBTE0TBX6VJ9KSG7K4D91SWJVDETNKQJXAFK9SAB3S31FZFA0Y0X22TKRXKCT7Z4GZCCRJJ12T1A5M4DWRTZDFRD3FE495NXHVPFM96KXMKH1HABTDDFZN0NWQ3NBJ6GNXD40NJ95E955X948JHBDJZWM3TEAK4XFJX8056XFDHVNXSF4VN14RR1WD1J5K7JPS61SKRNF3HT6NZA823PZW2KPV2KVBMMP615A922ZNJGVQDTW5TYWTK5DCBGG1YEKQRYF39NX9X722FZK98BTMHHH6WZFCKBT096G9BKSHSJW3VE8KKPCN8XGWYYPD3158HRKSA28BJQ9XJVVB6FDCGZ154WWGGSGW82BDYDH7ZHJBMS046AG0ND4ZCVR2JQ04002",
                  "value": "TESTKUDOS:0.1",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                },
                {
                  "master_sig": "FJPQKECRKVQSTB9Y983KDGD65Z1JHQKNSCC6YPMBN3Z4VW0AGC5MQM9BPB0YYD1SCMETPD6QB4X80HWE0ZDGWNZB1KND5TP567T4G3G",
                  "stamp_start": {
                    "t_ms": 1582484881000
                  },
                  "stamp_expire_withdraw": {
                    "t_ms": 1677092881000
                  },
                  "stamp_expire_deposit": {
                    "t_ms": 1740164881000
                  },
                  "stamp_expire_legal": {
                    "t_ms": 1897844881000
                  },
                  "denom_pub": "040000Y9PBY1HPBDD4KSK9PBA86TRY13JQ4284T4E0G4SEREQ8YM88PZHKW1ACKT1RTWVTBXX83G54NFVYRJQX9PTDXDJ1CXSS42G8NYMW97NA6NNNASV69W1JX39NTS1NVKXPW4WMBASATSNBTXHRT92FFN2NAJFGK876BNN3TPTH57C76ADAQV43VFF7CYAWWNYZAYGQQ1XY1NK34FJD778VFGYCZ1G9J8XPNB92ZKJBZEZKSNBRNH27GM5A736AFSGP7B4JSCGD0F4FMD1PDVB26MM9ZK8C1TDKXQ5DJ09AQQ55P7Q3A133ASPGBH6SCJTJYH8C9A451B0SP4GDX2ZFRSX5FP93PY4VKEB36KCAQ5E2MRZNWFB6T0JK0W7Z7NXP5FW2VQ4PNV7B2NQ3WFMCVRSDSV04002",
                  "value": "TESTKUDOS:1000",
                  "fee_withdraw": "TESTKUDOS:0.01",
                  "fee_deposit": "TESTKUDOS:0.01",
                  "fee_refresh": "TESTKUDOS:0.01",
                  "fee_refund": "TESTKUDOS:0.01"
                }
              ],
              "auditors": [],
              "list_issue_date": {
                "t_ms": 1592161681000
              },
              "eddsa_pub": "0FMRBH8FZYYMSQ2RHTYYGK2BV33JVSW6MTYCV7Y833GVNXFDYK10",
              "eddsa_sig": "2GB384567SZM9CM7RJT51N04D2ZK7NAHWZRT6BA0FFNXTAB71D4T1WVQTXZEPDM07X1MJ46ZBC189SCM4EG4V8TQJRP2WAZCKPAJJ2R"
            }""".trimIndent()
        }
        runCoroutine {
            val keys = Keys.fetch(httpClient, "https://exchange.test.taler.net/")
            assertEquals(expectedKeys, keys)
        }
    }


}