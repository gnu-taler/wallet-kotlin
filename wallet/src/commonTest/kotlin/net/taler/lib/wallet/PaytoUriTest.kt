/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class PaytoUriTest {

    @Test
    fun testFromString() {
        // wrong scheme
        var uri = "https://example.com/"
        assertNull(PaytoUri.fromString(uri))

        // incomplete scheme
        uri = "payto:blabla"
        assertNull(PaytoUri.fromString(uri))

        // proper URI
        uri = "payto://x-taler-bank/123"
        var parsedUri = PaytoUri.fromString(uri)
        assertNotNull(parsedUri)
        assertEquals("x-taler-bank", parsedUri.targetType)
        assertEquals("123", parsedUri.targetPath)

        // proper URI with incomplete query
        uri = "payto://x-taler-bank/123?foo"
        parsedUri = PaytoUri.fromString(uri)
        assertNotNull(parsedUri)
        assertEquals(0, parsedUri.params.size)

        // proper URI with two query param
        uri = "payto://x-taler-bank/123?foo=bar&hip=hop"
        parsedUri = PaytoUri.fromString(uri)
        assertNotNull(parsedUri)
        assertEquals(2, parsedUri.params.size)
        assertEquals("bar", parsedUri.params["foo"])
        assertEquals("hop", parsedUri.params["hip"])
    }

}
