/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp
import net.taler.lib.wallet.getMockHttpClient
import net.taler.lib.wallet.giveJsonResponse
import net.taler.lib.wallet.runCoroutine
import kotlin.test.Test
import kotlin.test.assertEquals

class WireTest {

    private val httpClient = getMockHttpClient()

    @Test
    fun testFetchWire() {
        val expectedWire = Wire(
            accounts = listOf(
                AccountInfo(
                    paytoUri = "payto://x-taler-bank/bank.test.taler.net/Exchange",
                    masterSig = "5DMYMQCEFWG7B21RAX8XQ585V689K8DSSR065F04E2JK6G9AF1WM8EVDCHHBMVWRY3P02EWEE4M6YVKPY6B43H2CPCWHDP13RM1WR10"
                )
            ),
            fees = mapOf(
                "x-taler-bank" to listOf(
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.04"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1577833200000),
                        endStamp = Timestamp(1609455600000),
                        signature = "9DS6TXPTM8ZBBTJS9VCRSD9FVS56ZY9EVWCF4HDA3Y2DNWSVMGS7XXPWE295EZ3E98KVV1SWDJ11CP0A0VDSRDZTM6RD2RPRG19ZA2G"
                    ),
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.04"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1609455600000),
                        endStamp = Timestamp(1640991600000),
                        signature = "81852REBNR3ZRQHKQ2FPT6CPACED0MA0CW4V9CPDS3NP2JX6X8BE5YE5W1AKR9XPASEXSKQH6FEXHP7VJB64XWA7FDGH5DKCD3Q700G"
                    ),
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.05"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1640991600000),
                        endStamp = Timestamp(1672527600000),
                        signature = "REYMSGH4QBNF339Q8TD5VJMMY6BV7KFTC1Y69YD69Y9E8Z5HXGNAKCQKT490MHBSF48894YADT1ATGDMSRZAQJJFVXF6HX9JEYDT61G"
                    ),
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.06"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1672527600000),
                        endStamp = Timestamp(1704063600000),
                        signature = "BXB47D936XT7XDHGA3VA3461CY1GMQWFPVMSBY01N5SN6PBCGYRS8HSY19FJ0P5HVX3TGS9TAHY9X7RP4BQHPM4DMMS30TJ0EKG5A3G"
                    ),
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.07"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1704063600000),
                        endStamp = Timestamp(1735686000000),
                        signature = "RFF1KV54BH9TJ8KBE8YEY8DM0R468PZYGW82G16P97EDHNN3XZVN4KK5E9CBZQ730WPJT0RKR3TTYPBWGTR0YQ064XZZDHJHHZN1418"
                    ),
                    WireFee(
                        wireFee = Amount.fromJSONString("TESTKUDOS:0.08"),
                        closingFee = Amount.fromJSONString("TESTKUDOS:0.01"),
                        startStamp = Timestamp(1735686000000),
                        endStamp = Timestamp(1767222000000),
                        signature = "Q89VKJ54MF3DVG0NKK4N6VB96NCT0PRSTNBJ0SSB42SQTHB10JC68XJSDM6PRBBPEJ8CHDE9VVRZWW20VFSZFDTRA332JKDSBBFWY1G"
                    )
                )
            )
        )
        giveJsonResponse("https://exchange.test.taler.net/wire") {
            """{
              "accounts": [
                {
                  "payto_uri": "payto://x-taler-bank/bank.test.taler.net/Exchange",
                  "master_sig": "5DMYMQCEFWG7B21RAX8XQ585V689K8DSSR065F04E2JK6G9AF1WM8EVDCHHBMVWRY3P02EWEE4M6YVKPY6B43H2CPCWHDP13RM1WR10"
                }
              ],
              "fees": {
                "x-taler-bank": [
                  {
                    "wire_fee": "TESTKUDOS:0.04",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1577833200000
                    },
                    "end_date": {
                      "t_ms": 1609455600000
                    },
                    "sig": "9DS6TXPTM8ZBBTJS9VCRSD9FVS56ZY9EVWCF4HDA3Y2DNWSVMGS7XXPWE295EZ3E98KVV1SWDJ11CP0A0VDSRDZTM6RD2RPRG19ZA2G"
                  },
                  {
                    "wire_fee": "TESTKUDOS:0.04",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1609455600000
                    },
                    "end_date": {
                      "t_ms": 1640991600000
                    },
                    "sig": "81852REBNR3ZRQHKQ2FPT6CPACED0MA0CW4V9CPDS3NP2JX6X8BE5YE5W1AKR9XPASEXSKQH6FEXHP7VJB64XWA7FDGH5DKCD3Q700G"
                  },
                  {
                    "wire_fee": "TESTKUDOS:0.05",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1640991600000
                    },
                    "end_date": {
                      "t_ms": 1672527600000
                    },
                    "sig": "REYMSGH4QBNF339Q8TD5VJMMY6BV7KFTC1Y69YD69Y9E8Z5HXGNAKCQKT490MHBSF48894YADT1ATGDMSRZAQJJFVXF6HX9JEYDT61G"
                  },
                  {
                    "wire_fee": "TESTKUDOS:0.06",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1672527600000
                    },
                    "end_date": {
                      "t_ms": 1704063600000
                    },
                    "sig": "BXB47D936XT7XDHGA3VA3461CY1GMQWFPVMSBY01N5SN6PBCGYRS8HSY19FJ0P5HVX3TGS9TAHY9X7RP4BQHPM4DMMS30TJ0EKG5A3G"
                  },
                  {
                    "wire_fee": "TESTKUDOS:0.07",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1704063600000
                    },
                    "end_date": {
                      "t_ms": 1735686000000
                    },
                    "sig": "RFF1KV54BH9TJ8KBE8YEY8DM0R468PZYGW82G16P97EDHNN3XZVN4KK5E9CBZQ730WPJT0RKR3TTYPBWGTR0YQ064XZZDHJHHZN1418"
                  },
                  {
                    "wire_fee": "TESTKUDOS:0.08",
                    "closing_fee": "TESTKUDOS:0.01",
                    "start_date": {
                      "t_ms": 1735686000000
                    },
                    "end_date": {
                      "t_ms": 1767222000000
                    },
                    "sig": "Q89VKJ54MF3DVG0NKK4N6VB96NCT0PRSTNBJ0SSB42SQTHB10JC68XJSDM6PRBBPEJ8CHDE9VVRZWW20VFSZFDTRA332JKDSBBFWY1G"
                  }
                ]
              },
              "master_public_key": "DY95EXAHQ2BKM2WK9YHZHYG1R7PPMMJPY14FNGP662DAKE35AKQG"
            }""".trimIndent()
        }

        runCoroutine {
            val wire = Wire.fetch(httpClient, "https://exchange.test.taler.net/")
            assertEquals(expectedWire, wire)
        }
    }

}
