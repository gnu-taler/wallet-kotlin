/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.operations

import net.taler.lib.common.Amount
import net.taler.lib.wallet.exchange.DenominationSelectionInfo
import net.taler.lib.wallet.exchange.Denominations.denomination0d01
import net.taler.lib.wallet.exchange.Denominations.denomination0d1
import net.taler.lib.wallet.exchange.Denominations.denomination1
import net.taler.lib.wallet.exchange.Denominations.denomination10
import net.taler.lib.wallet.exchange.Denominations.denomination2
import net.taler.lib.wallet.exchange.Denominations.denomination4
import net.taler.lib.wallet.exchange.Denominations.denomination5
import net.taler.lib.wallet.exchange.Denominations.denomination8
import net.taler.lib.wallet.exchange.SelectedDenomination
import net.taler.lib.wallet.getMockHttpClient
import net.taler.lib.wallet.giveJsonResponse
import net.taler.lib.wallet.operations.Withdraw.BankDetails
import net.taler.lib.wallet.runCoroutine
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals

internal class WithdrawTest {

    private val httpClient = getMockHttpClient()
    private val withdraw = Withdraw(httpClient)

    @Ignore // live test that requires internet connectivity and a working exchange
    @Test
    fun testLiveUpdate() {
        runCoroutine {
            val withdraw = Withdraw() // use own instance without mocked HTTP client
            val url = "http://exchange.test.taler.net/"
            val amount = Amount("TESTKUDOS", 5, 0)
            val details = withdraw.getWithdrawalDetails(url, amount)
            assertEquals(url, details.exchange.baseUrl)
        }
    }

    @Test
    fun getBankWithdrawalInfo() {
        val bankDetails = BankDetails(
            amount = Amount.fromJSONString("TESTKUDOS:5"),
            confirmTransferUrl = "https://bank.test.taler.net/confirm-withdrawal/9b51c1dd-db41-4b5f-97d9-1071d5dd8091",
            extractedStatusUrl = "https://bank.test.taler.net/api/withdraw-operation/9b51c1dd-db41-4b5f-97d9-1071d5dd8091",
            selectionDone = false,
            senderPaytoUri = "payto://x-taler-bank/bank.test.taler.net/test",
            suggestedExchange = "https://exchange.test.taler.net/",
            transferDone = false,
            wireTypes = listOf("x-taler-bank")
        )
        giveJsonResponse(bankDetails.extractedStatusUrl) {
            """{
                "selection_done": ${bankDetails.selectionDone},
                "transfer_done": ${bankDetails.transferDone},
                "amount": "${bankDetails.amount.toJSONString()}",
                "wire_types": ["${bankDetails.wireTypes[0]}"],
                "sender_wire": "${bankDetails.senderPaytoUri}",
                "suggested_exchange": "${bankDetails.suggestedExchange}",
                "confirm_transfer_url": "${bankDetails.confirmTransferUrl}"
            }""".trimIndent()
        }
        runCoroutine {
            val details =
                withdraw.getBankInfo("taler://withdraw/bank.test.taler.net/9b51c1dd-db41-4b5f-97d9-1071d5dd8091")
            assertEquals(bankDetails, details)
        }
    }

    @Test
    fun testGetDenominationSelection() {
        val allDenominations = listOf(
            denomination0d1,
            denomination0d01,  // unsorted list to test sort as well
            denomination10,
            denomination8,
            denomination5,
            denomination4,
            denomination2,
            denomination1
        )
        // select denominations for 10 TESTKUDOS
        val value10 = Amount(currency = "TESTKUDOS", value = 10, fraction = 0)
        val expectedSelectionInfo10 = DenominationSelectionInfo(
            totalCoinValue = Amount(currency = "TESTKUDOS", value = 9, fraction = 82000000),
            totalWithdrawCost = Amount(currency = "TESTKUDOS", value = 9, fraction = 99000000),
            selectedDenominations = listOf(
                SelectedDenomination(1, denomination8),
                SelectedDenomination(1, denomination1),
                SelectedDenomination(8, denomination0d1),
                SelectedDenomination(2, denomination0d01)
            )
        )
        assertEquals(expectedSelectionInfo10, withdraw.getDenominationSelection(value10, allDenominations))

        // select denominations for 5.5 TESTKUDOS
        val value5d5 = Amount(currency = "TESTKUDOS", value = 5, fraction = 50000000)
        val expectedSelectionInfo5d5 = DenominationSelectionInfo(
            totalCoinValue = Amount(currency = "TESTKUDOS", value = 5, fraction = 42000000),
            totalWithdrawCost = Amount(currency = "TESTKUDOS", value = 5, fraction = 49000000),
            selectedDenominations = listOf(
                SelectedDenomination(1, denomination5),
                SelectedDenomination(4, denomination0d1),
                SelectedDenomination(2, denomination0d01)
            )
        )
        assertEquals(expectedSelectionInfo5d5, withdraw.getDenominationSelection(value5d5, allDenominations))

        // select denominations for 23.42 TESTKUDOS
        val value23d42 = Amount(currency = "TESTKUDOS", value = 23, fraction = 42000000)
        val expectedSelectionInfo23d42 = DenominationSelectionInfo(
            totalCoinValue = Amount(currency = "TESTKUDOS", value = 23, fraction = 31000000),
            totalWithdrawCost = Amount(currency = "TESTKUDOS", value = 23, fraction = 42000000),
            selectedDenominations = listOf(
                SelectedDenomination(2, denomination10),
                SelectedDenomination(1, denomination2),
                SelectedDenomination(1, denomination1),
                SelectedDenomination(3, denomination0d1),
                SelectedDenomination(1, denomination0d01)
            )
        )
        assertEquals(expectedSelectionInfo23d42, withdraw.getDenominationSelection(value23d42, allDenominations))
    }

}
