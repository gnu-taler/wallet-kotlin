/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.api

import net.taler.lib.common.Amount
import net.taler.lib.wallet.runCoroutine
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Ignore // Live-Test which fails when test environment changes or is not available
class WalletApiTest {

    private val api = WalletApiImpl()

    private val exchangeBaseUrl = "https://exchange.test.taler.net/"
    private val withdrawUri =
        "taler://withdraw/bank.test.taler.net/8adabbf8-fe61-4d31-a2d1-89350b5be8fa"
    private val paytoUris = listOf("payto://x-taler-bank/bank.test.taler.net/Exchange")
    private val tosEtag = "0"

    @Test
    fun testGetVersions() {
        api.getVersions()
    }

    @Test
    fun testGetWithdrawalDetails() = runCoroutine {
        val detailsForUri = api.getWithdrawalDetailsForUri(withdrawUri)
        assertEquals(Amount("TESTKUDOS", 5, 0), detailsForUri.amount)
        assertEquals(exchangeBaseUrl, detailsForUri.defaultExchangeBaseUrl)

        val detailsForAmount = api.getWithdrawalDetailsForAmount(
            detailsForUri.defaultExchangeBaseUrl!!,
            detailsForUri.amount
        )
        assertEquals(Amount("TESTKUDOS", 5, 0), detailsForAmount.amountRaw)
        assertEquals(Amount("TESTKUDOS", 4, 80000000), detailsForAmount.amountEffective)
        assertEquals(false, detailsForAmount.tosAccepted)
    }

    @Test
    fun testExchangeManagement() = runCoroutine {
        // initially we have no exchanges
        assertTrue(api.listExchanges().isEmpty())

        // test exchange gets added correctly
        val exchange = api.addExchange(exchangeBaseUrl)
        assertEquals(exchangeBaseUrl, exchange.exchangeBaseUrl)
        assertEquals("TESTKUDOS", exchange.currency)
        assertEquals(paytoUris, exchange.paytoUris)

        // added exchange got added to list
        val exchanges = api.listExchanges()
        assertEquals(1, exchanges.size)
        assertEquals(exchange, exchanges[0])

        // ToS are not accepted
        val tosResult = api.getExchangeTos(exchangeBaseUrl)
        assertEquals(null, tosResult.acceptedEtag)
        assertEquals(tosEtag, tosResult.currentEtag)
        assertTrue(tosResult.tos.length > 100)

        // withdrawal details also show ToS as not accepted
        val withdrawalDetails =
            api.getWithdrawalDetailsForAmount(exchangeBaseUrl, Amount.zero("TESTKUDOS"))
        assertEquals(false, withdrawalDetails.tosAccepted)

        // accept ToS
        api.setExchangeTosAccepted(exchangeBaseUrl, tosResult.currentEtag)
        val newTosResult = api.getExchangeTos(exchangeBaseUrl)
        assertEquals(newTosResult.currentEtag, newTosResult.acceptedEtag)

        // withdrawal details now show ToS as accepted as well
        val newWithdrawalDetails =
            api.getWithdrawalDetailsForAmount(exchangeBaseUrl, Amount.zero("TESTKUDOS"))
        assertEquals(true, newWithdrawalDetails.tosAccepted)
    }

}
