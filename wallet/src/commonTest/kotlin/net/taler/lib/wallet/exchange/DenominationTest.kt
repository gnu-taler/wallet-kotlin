/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.wallet.exchange

import net.taler.lib.common.Amount
import net.taler.lib.common.Timestamp
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedBad
import net.taler.lib.wallet.exchange.DenominationStatus.VerifiedGood
import net.taler.lib.wallet.exchange.Denominations.denomination1
import net.taler.lib.wallet.exchange.Denominations.denomination10
import net.taler.lib.wallet.exchange.Denominations.denomination2
import net.taler.lib.wallet.exchange.Denominations.denomination5
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class DenominationTest {

    @Test
    fun testGetEarliestDepositExpiry() {
        // empty selection info never expires
        val infoEmpty = DenominationSelectionInfo(
            totalCoinValue = Amount.zero("TESTKUDOS"),
            totalWithdrawCost = Amount.zero("TESTKUDOS"),
            selectedDenominations = emptyList()
        )
        assertEquals(Timestamp.never(), infoEmpty.getEarliestDepositExpiry())

        // earliest expiry of single denomination is that of the denomination
        val info1 = infoEmpty.copy(
            selectedDenominations = listOf(SelectedDenomination(1, denomination10))
        )
        assertEquals(denomination10.stampExpireDeposit, info1.getEarliestDepositExpiry())

        // denomination that expires earlier gets selected
        val info2 = infoEmpty.copy(
            selectedDenominations = listOf(
                SelectedDenomination(3, denomination5.copy(stampExpireDeposit = Timestamp(42))),
                SelectedDenomination(2, denomination2.copy(stampExpireDeposit = Timestamp(2))),
                SelectedDenomination(1, denomination1.copy(stampExpireDeposit = Timestamp(1)))
            )
        )
        assertEquals(Timestamp(1), info2.getEarliestDepositExpiry())

        // denomination that expires at all is earlier than the one that never expires
        val info3 = infoEmpty.copy(
            selectedDenominations = listOf(
                SelectedDenomination(2, denomination2.copy(stampExpireDeposit = Timestamp.never())),
                SelectedDenomination(1, denomination1.copy(stampExpireDeposit = Timestamp(1)))
            )
        )
        assertEquals(Timestamp(1), info3.getEarliestDepositExpiry())
    }

    @Test
    fun testIsWithdrawableDenomination() {
        // denomination is withdrawable
        assertTrue(denomination1.isWithdrawable())
        // denomination is withdrawable when VerifiedGood
        assertTrue(denomination1.copy(status = VerifiedGood).isWithdrawable())
        // fails with VerifiedBad
        assertFalse(denomination1.copy(status = VerifiedBad).isWithdrawable())
        // fails when revoked
        assertFalse(denomination1.copy(isRevoked = true).isWithdrawable())
        // fails when not started
        assertFalse(denomination1.copy(stampStart = Timestamp(Timestamp.now().ms + 9999)).isWithdrawable())
        // fails when expired
        assertFalse(denomination1.copy(stampExpireWithdraw = Timestamp.now()).isWithdrawable())
        // fails when almost expired
        assertFalse(denomination1.copy(stampExpireWithdraw = Timestamp(Timestamp.now().ms + 5000)).isWithdrawable())
        // succeeds when not quite expired
        assertTrue(denomination1.copy(stampExpireWithdraw = Timestamp(Timestamp.now().ms + 51000)).isWithdrawable())
    }

}
