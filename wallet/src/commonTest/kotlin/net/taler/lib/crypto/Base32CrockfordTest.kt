/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.crypto

import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalStdlibApi
class Base32CrockfordTest {

    private class TestVector(val value: ByteArray, val encoding: List<String>)

    private val vectors = listOf(
        TestVector(byteArrayOf(0), listOf("00", "0O", "0o")),
        TestVector(byteArrayOf(0), listOf("00", "0O", "0o")),
        TestVector(byteArrayOf(1), listOf("04")),
        TestVector(byteArrayOf(2), listOf("08")),
        TestVector(byteArrayOf(3), listOf("0C")),
        TestVector(byteArrayOf(4), listOf("0G")),
        TestVector(byteArrayOf(5), listOf("0M")),
        TestVector(byteArrayOf(6), listOf("0R")),
        TestVector(byteArrayOf(7), listOf("0W")),
        TestVector(byteArrayOf(8), listOf("10")),
        TestVector(byteArrayOf(9), listOf("14")),
        TestVector(byteArrayOf(10), listOf("18")),
        TestVector(byteArrayOf(11), listOf("1C")),
        TestVector(byteArrayOf(12), listOf("1G")),
        TestVector(byteArrayOf(13), listOf("1M")),
        TestVector(byteArrayOf(14), listOf("1R")),
        TestVector(byteArrayOf(15), listOf("1W")),
        TestVector(byteArrayOf(16), listOf("20")),
        TestVector(byteArrayOf(17), listOf("24")),
        TestVector(byteArrayOf(18), listOf("28")),
        TestVector(byteArrayOf(19), listOf("2C")),
        TestVector(byteArrayOf(20), listOf("2G")),
        TestVector(byteArrayOf(21), listOf("2M")),
        TestVector(byteArrayOf(22), listOf("2R")),
        TestVector(byteArrayOf(23), listOf("2W")),
        TestVector(byteArrayOf(24), listOf("30")),
        TestVector(byteArrayOf(25), listOf("34")),
        TestVector(byteArrayOf(26), listOf("38")),
        TestVector(byteArrayOf(27), listOf("3C")),
        TestVector(byteArrayOf(28), listOf("3G")),
        TestVector(byteArrayOf(29), listOf("3M")),
        TestVector(byteArrayOf(30), listOf("3R")),
        TestVector(byteArrayOf(31), listOf("3W")),
        TestVector(byteArrayOf(0, 0), listOf("0000", "oooo", "OOOO", "0oO0")),
        TestVector(byteArrayOf(1, 0), listOf("0400", "o4oo", "O4OO", "04oO")),
        TestVector(byteArrayOf(0, 1), listOf("000G", "ooog", "OOOG", "0oOg")),
        TestVector(byteArrayOf(1, 1), listOf("040G", "o4og", "O4og", "04Og")),
        TestVector(byteArrayOf(136.toByte(), 64), listOf("H100", "hio0", "HLOo")),
        TestVector(byteArrayOf(139.toByte(), 188.toByte()), listOf("HEY0", "heyo", "HeYO")),
        TestVector(byteArrayOf(54, 31, 127), listOf("6RFQY", "6rfqy")),
        TestVector(
            byteArrayOf(72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100, 33),
            listOf("91JPRV3F41BPYWKCCGGG", "91jprv3f41bpywkccggg", "9Ljprv3f4ibpywkccggg")
        ),
        TestVector(
            byteArrayOf(139.toByte(), 130.toByte(), 16, 112, 24, 11, 64),
            listOf("HE110W0R1D00", "helloworld00", "heiiOw0RidoO")
        ),
        TestVector(
            byteArrayOf(139.toByte(), 130.toByte(), 16, 112, 24, 11),
            listOf("HE110W0R1C", "helloworlc", "heiiOw0RiC")
        ),
        TestVector(
            byteArrayOf(139.toByte(), 130.toByte(), 16, 112, 24, 11, 0),
            listOf("HE110W0R1C00", "helloworlc00", "heiiOw0RiC00")
        )
    )

    @Test
    fun testEncode() {
        for (vector in vectors) {
            assertEquals(vector.encoding[0], Base32Crockford.encode(vector.value))
        }
    }

    @Test
    fun testDecode() {
        for (vector in vectors) {
            for (encoding in vector.encoding) {
                assertTrue(vector.value contentEquals Base32Crockford.decode(encoding))
            }
        }
    }

    @Ignore // TODO
    @Test
    fun testDecodeFuck() {
        val bytes = byteArrayOf(0x7e, 0xd9.toByte())
        assertTrue(bytes contentEquals Base32Crockford.decode("FUCK"))
        assertTrue(bytes contentEquals Base32Crockford.decode("FuCk"))
        assertTrue(bytes contentEquals Base32Crockford.decode("fUcK"))
        assertTrue(bytes contentEquals Base32Crockford.decode("FVCK"))
    }

    @Test
    fun testEncodingDecoding() {
        val input = "Hello, World"
        val encoded = Base32Crockford.encode(input.encodeToByteArray())
        val decoded = Base32Crockford.decode(encoded)
        val output = decoded.decodeToString()
        assertEquals(input, output)
    }

}
