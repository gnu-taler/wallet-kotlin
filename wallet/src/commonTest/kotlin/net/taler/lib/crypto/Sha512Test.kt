/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.crypto

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals

class Sha512Test {

    private val crypto = CryptoFactory.getCrypto()

    @Test
    fun testAbc() {
        assertEquals(
            "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f",
            crypto.sha512("abc".encodeToByteArray()).toHexString()
        )
    }

    @Test
    fun testEmptyString() {
        assertEquals(
            "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e",
            crypto.sha512("".encodeToByteArray()).toHexString()
        )
    }

    @Test
    fun testAbc448bits() {
        assertEquals(
            "204a8fc6dda82f0a0ced7beb8e08a41657c16ef468b228a8279be331a703c33596fd15c13b1b07f9aa1d3bea57789ca031ad85c7a71dd70354ec631238ca3445",
            crypto.sha512("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".encodeToByteArray()).toHexString()
        )
    }

    @Test
    fun testAbc896bits() {
        assertEquals(
            "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909",
            crypto.sha512("abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu".encodeToByteArray())
                .toHexString()
        )
    }

    @Test
    fun testAMillionAs() {
        val input = ByteArray(1_000_000) { 0x61 }
        assertEquals(
            "e718483d0ce769644e2e42c7bc15b4638e1f98b13b2044285632a803afa973ebde0ff244877ea60a4cb0432ce577c31beb009c5c2c49aa2e4eadb217ad8cc09b",
            crypto.sha512(input).toHexString()
        )
    }

    @Test
    fun testExchangeTvgHashCode() {
        val input = "91JPRV3F5GG4EKJN41A62V35E8"
        val output =
            "CW96WR74JS8T53EC8GKSGD49QKH4ZNFTZXDAWMMV5GJ1E4BM6B8GPN5NVHDJ8ZVXNCW7Q4WBYCV61HCA3PZC2YJD850DT29RHHN7ESR"
        assertEquals(output, Base32Crockford.encode(crypto.sha512(Base32Crockford.decode(input))))
    }

    @Test
    fun testIncrementalHashing() {
        val n = 1024
        val d = Random.nextBytes(n)

        val h1 = crypto.sha512(d)
        val h2 = crypto.getHashSha512State().update(d).final()
        assertEquals(Base32Crockford.encode(h1), Base32Crockford.encode(h2))

        val s = crypto.getHashSha512State()
        for (i in 0 until n) {
            val b = ByteArray(1)
            b[0] = d[i]
            s.update(b)
        }
        val h3 = s.final()
        assertEquals(Base32Crockford.encode(h1), Base32Crockford.encode(h3))
    }

    @Test
    fun testIncrementalHashing2() {
        val n = 10
        val d = Random.nextBytes(n)

        val h1 = crypto.sha512(d)
        val h2 = crypto.getHashSha512State().update(d).final()
        assertEquals(Base32Crockford.encode(h1), Base32Crockford.encode(h2))

        val s = crypto.getHashSha512State()
        for (i in 0 until n) {
            val b = ByteArray(1)
            b[0] = d[i]
            s.update(b)
        }
        val h3 = s.final()
        assertEquals(Base32Crockford.encode(h1), Base32Crockford.encode(h3))
    }

}
