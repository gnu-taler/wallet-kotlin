/*
 * This file is part of GNU Taler
 * (C) 2020 Taler Systems S.A.
 *
 * GNU Taler is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 *
 * GNU Taler is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * GNU Taler; see the file COPYING.  If not, see <http://www.gnu.org/licenses/>
 */

package net.taler.lib.crypto

import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EllipticCurveTest {

    private val crypto = CryptoFactory.getCrypto()

    @Test
    fun testExchangeTvgEddsaKey() {
        val pri = "9TM70AKDTS57AWY9JK2J4TMBTMW6K62WHHGZWYDG0VM5ABPZKD40"
        val pub = "8GSJZ649T2PXMKZC01Y4ANNBE7MF14QVK9SQEC4E46ZHKCVG8AS0"
        val pubBytes = crypto.eddsaGetPublic(Base32Crockford.decode(pri))
        assertEquals(pub, Base32Crockford.encode(pubBytes))

        val pri2 = "C9C5X5JS2SWRTSX5AQBPFA4S2DFD6V9Q04XTTEZDQ5XSSQK2P39G"
        val pub2 = "6WC3MPYM5XKPKRA2Z6SYB81CPFV3E7EC6S2GVE095X8XH63QTZCG"
        val pub2Bytes = crypto.eddsaGetPublic(Base32Crockford.decode(pri2))
        assertEquals(pub2, Base32Crockford.encode(pub2Bytes))
    }

    @Test
    fun testExchangeTvgEcdheKey() {
        val pri = "X4T4N0M8PVQXQEBW2BA7049KFSM7J437NSDFC6GDNM3N5J9367A0"
        val pub = "M997P494MS6A95G1P0QYWW2VNPSHSX5Q6JBY5B9YMNYWP0B50X3G"
        val pubBytes = crypto.ecdheGetPublic(Base32Crockford.decode(pri))
        assertEquals(pub, Base32Crockford.encode(pubBytes))

        val pri2 = "MB8ZSQFTVPX4V0MGT3BA1PQS12NJ7KH33DA8D22NCWTNA5BHH2YG"
        val pub2 = "8T2CN1W8G3XZE9C9158A5ASS74117GK1XQ1XAX5SGBFDGHS8H5W0"
        val pub2Bytes = crypto.ecdheGetPublic(Base32Crockford.decode(pri2))
        assertEquals(pub2, Base32Crockford.encode(pub2Bytes))
    }

    @Test
    fun testCreateEddsaKeyPair() {
        val pair1 = crypto.createEddsaKeyPair()
        val pair2 = crypto.createEddsaKeyPair()
        assertFalse(pair1.privateKey contentEquals pair2.privateKey)
        assertFalse(pair1.publicKey contentEquals pair2.publicKey)
    }

    @Test
    fun testCreateEcdheKeyPair() {
        val pair1 = crypto.createEcdheKeyPair()
        val pair2 = crypto.createEcdheKeyPair()
        assertFalse(pair1.privateKey contentEquals pair2.privateKey)
        assertFalse(pair1.publicKey contentEquals pair2.publicKey)
    }

    @Test
    @ExperimentalStdlibApi
    fun testEddsaSignAndVerify() {
        val msg = "Hallo world!".encodeToByteArray()
        val pri = "9TM70AKDTS57AWY9JK2J4TMBTMW6K62WHHGZWYDG0VM5ABPZKD40"
        val expectedSig =
            "Z6H76JXPJFP3JBGSF54XBF0BVXDJ0CJBK4YT9GVR1AT916ZD57KP53YZN5G67A4YN95WGMZKQW7744483P5JDF06B6S7TMK195QGP20"
        val sig = crypto.eddsaSign(msg, Base32Crockford.decode(pri))
        assertEquals(expectedSig, Base32Crockford.encode(sig))

        val pub = crypto.eddsaGetPublic(Base32Crockford.decode(pri))
        assertTrue(crypto.eddsaVerify(msg, sig, pub))

        val wrongSig = Random.nextBytes(64)
        assertFalse(crypto.eddsaVerify(msg, wrongSig, pub))

        val wrongPub = Random.nextBytes(32)
        assertFalse(crypto.eddsaVerify(msg, sig, wrongPub))

        val wrongMsg = Random.nextBytes(16)
        assertFalse(crypto.eddsaVerify(wrongMsg, sig, pub))

        val pri2 = "T4NK2VVZZ1ZF6EBGEQHRK7KY67PCEVAZC5YHYH612XG5R6NJXSB0"
        val pub2 = "2X3PSPT7D6TEM97R98C0DHZREFVAVA3XTH11D5A2Z2K7GBKQ7AEG"
        val data2 =
            "00000J00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
        val expectedSig2 =
            "RFC59ZWJB88TD03F4GTQD3RWKH3KKRVEY2T1VN7CFEF51SCZ4BX8HGHC708GW1SN7Y4XFEDE3A5PFZEJ7PR7V09YZXX10DM608T1W20"
        val sig2 = crypto.eddsaSign(Base32Crockford.decode(data2), Base32Crockford.decode(pri2))
        assertEquals(expectedSig2, Base32Crockford.encode(sig2))
        assertTrue(crypto.eddsaVerify(Base32Crockford.decode(data2), sig2, Base32Crockford.decode(pub2)))
    }

    @Test
    fun testExchangeTvgEddsaEcdh() {
        val ecdhePrivateKey = "4AFZWMSGTVCHZPQ0R81NWXDCK4N58G7SDBBE5KXE080Y50370JJG"
        val ecdhePublicKey = "FXFN5GPAFTKVPWJDPVXQ87167S8T82T5ZV8CDYC0NH2AE14X0M30"
        val ecdhePublicKeyBytes = crypto.ecdheGetPublic(Base32Crockford.decode(ecdhePrivateKey))
        assertEquals(ecdhePublicKey, Base32Crockford.encode(ecdhePublicKeyBytes))

        val eddsaPrivateKey = "1KG54M8T3X8BSFSZXCR3SQBSR7Y9P53NX61M864S7TEVMJ2XVPF0"
        val eddsaPublicKey = "7BXWKG6N224C57RTDV8XEAHR108HG78NMA995BE8QAT5GC1S7E80"
        val eddsaPublicKeyBytes = crypto.eddsaGetPublic(Base32Crockford.decode(eddsaPrivateKey))
        assertEquals(eddsaPublicKey, Base32Crockford.encode(eddsaPublicKeyBytes))

        val keyMaterial =
            "PKZ42Z56SVK2796HG1QYBRJ6ZQM2T9QGA3JA4AAZ8G7CWK9FPX175Q9JE5P0ZAX3HWWPHAQV4DPCK10R9X3SAXHRV0WF06BHEC2ZTKR"
        val keyMaterial1Bytes = crypto.keyExchangeEddsaEcdhe(
            Base32Crockford.decode(eddsaPrivateKey),
            Base32Crockford.decode(ecdhePublicKey)
        )
        assertEquals(keyMaterial, Base32Crockford.encode(keyMaterial1Bytes))

        val keyMaterial2Bytes = crypto.keyExchangeEcdheEddsa(
            Base32Crockford.decode(ecdhePrivateKey),
            Base32Crockford.decode(eddsaPublicKey)
        )
        assertEquals(keyMaterial, Base32Crockford.encode(keyMaterial2Bytes))

        val ecdhePrivateKey2 = "3AMRJ1KC87VWX9MVQMW8MB9YVX5DMS6P5V2SYZNCZ44XVMSDVEFG"
        val ecdhePublicKey2 = "CFFVCRFTNP7701KZC7187BC42MGSVCMBWK38F23E7T9VVK9D41Q0"
        val ecdhePublicKeyBytes2 = crypto.ecdheGetPublic(Base32Crockford.decode(ecdhePrivateKey2))
        assertEquals(ecdhePublicKey2, Base32Crockford.encode(ecdhePublicKeyBytes2))

        val eddsaPrivateKey2 = "8AZTVPCXNCVBKJVQ6XS7Z38KB9JB38WJ7Z80F9FTCX1WHZACTXJ0"
        val eddsaPublicKey2 = "T3F1VYAVZX3DVCNJTB3BNQTRZGXN446QP2VD847CB6N5V75RT2B0"
        val eddsaPublicKeyBytes2 = crypto.eddsaGetPublic(Base32Crockford.decode(eddsaPrivateKey2))
        assertEquals(eddsaPublicKey2, Base32Crockford.encode(eddsaPublicKeyBytes2))

        val keyMaterial2 =
            "FPQSHC6MWWJGEPXXVF76Q8C4PBJFE59RSGRGFQ3Z6ESR67HMG7FAK6JTCQ4ZKHSVNNCF53DX8FY55EA2193N2A6KD510AEV5TBVZRJR"
        val keyMaterial1Bytes2 = crypto.keyExchangeEddsaEcdhe(
            Base32Crockford.decode(eddsaPrivateKey2),
            Base32Crockford.decode(ecdhePublicKey2)
        )
        assertEquals(keyMaterial2, Base32Crockford.encode(keyMaterial1Bytes2))

        val keyMaterial2Bytes2 = crypto.keyExchangeEcdheEddsa(
            Base32Crockford.decode(ecdhePrivateKey2),
            Base32Crockford.decode(eddsaPublicKey2)
        )
        assertEquals(keyMaterial2, Base32Crockford.encode(keyMaterial2Bytes2))
    }

}
